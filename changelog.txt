﻿Version 1.9.1f(hotfix):
-Fix station price
-Nerf Spreadgun MIRV fluxcost to 240 from 190
-Nerf Spreadgun MIRV range to 900 from 1200
-Nerf Spreadgun MIRV flight time to 7 from 10
-Nerf and Fix the Spreadgun MIRV AI


Version 1.9.1d(hotfix):
-Fix Nexerelin compatibility Tariff.
-Tariff will be nerfed on friendly/cooperative.

Version 1.9.1c(hotfix):
-Fix a system weapon who appears on the market


Version 1.9.1b(hotfix):(Need new save except Nex random system)
-Fix tarif of Artefact. (Work only on without Nex or without random system)
-Remove Black market of Looters, Noir. (Work only on without Nex or without random system)
-Add Looters market(a black looters market) on Noir market. (Work only on without Nex or without random system)
-Increase fleet point of Ame to 40 from 60.
-Increase maintenance/supply deploy to 60/70 from 70/80


Version 1.9.1:
-Fix hatred on Artefact.
-Reduce market size from 7 to 6.
-Increase tarif to 100%. And toll per 100%.
-Increase cargo and fuel of Heart-Z per 200 units.
-Increase fleet point of Ame to 60 from 30.
-Increase maintenance/supply deploy to 70/80 from 50/60
-Increase topspeed  of Ame to 35 from 40
-Reduce hull of Ame to 22 000 from 25000
-Reduce armor of Ame to 1400 from 1600

Version 1.9c:
-Update for 0.9.1a
-Like Noir ships have a bubble shield, the PD become a machine gun-like.
-Nerf refit time of bomber.
-Fix price of ships
-Reduce per ten Noir and Looters D-ship of the pools of pirates.
-Reduce per five to see Looters ships on Scavengers pools.

Version 1.9b_b: (Not sure if break your save game, need to try)
-Fix a system weapon available on the market. 
-Fix the "low" radius of the right click shipsystem of Looters ships.
-Add HUD help for the Looters.
-Require MagicLib
-Fix crash about a null.
-Nerf the Noir small PD(maybe a buff for some people?) per remove the "not aim"


Version 1.9b: Break save
-Fix shield always alive when the ship is died.
-clean useless stuff on graphic folder.
-Modify sprite of the Ame capital( reduced the tail) for save 3/4 of the VRAM used for him. (Yeah, i have just removed 30 pixels for gain so much)
-Add Noir/Looters blueprint on Nex start.
-Remove normal map for Looters. (The Lotus shield do the job)
-Reduce the VRAM used per Artefact.
-Add 2 destroyer and 1 cruiser.
-Change the Looters Lotus shield graphic.
-Clean-up Artefact(caused break-save)
-Fix image for pixel rotation.

Version 1.9alpha: BREAK SAVE
(I need feedback, thank)
-Modify the Looters shield per a better shield.
-Nerf the Looters shipsystem, one time now, decrease range, increase flux cost.
-Fix the Looters shipsystem who caused overload.
-Change the graphic of the Looters shipsystem.
-Fix color of the projectile of the vulcan, who is near to be just invisible.
-Down the topspeed of the M12-II to 55 from 75
-Replace the Shadow per a different fighter.
-Remove the weapon flux/Stunlock of the game.
-Modify the large of a drone module of the station level 3. REDUCE THE DIFFICULTY OF 50%.
-Fix shield rotation.
-Increase the texture shield radius.


Version 1.8d:
-TODO: Fix the starsystem, where some planet is bad.
-Nex compatibility

Version 1.8c:
-Fix Blueprint
-Fix Noir and Looters factions   weapons not properly loaded on them.

Version 1.8b:
-add a Noir station and Looters Station
-Fix the mod_info


Version 1.8:
-0.9 compatibility
-Graphiclib is now not required, just compatible.


Version 1.7b Beta:(Maybe break save?) 
Hullmods:
-Artefact System:
	New Icon
-S-Armor:
	Range reduced to 0/5/10/15% from 20%
	Emp damage reduced to 35% from 50%
	Weapon health reduced to 35% from 50%
	HE Damage reduced to 10% from 50%
	Damage reduced to 10% from 0%
Weapons:
-Change name of 16 MI, 32 MJ, 64 MJ Railgun per MI-16, MJ-32 and MJ-64 Railgun
-Change the name of the lightning beam to Velocity Beam
-MJ-32 Railgun: 
        Op Cost: 7 from 9
        Range: 750 from 850
-M224 Mortar:
        Burst size: 6 from 5
	Recoil : 5 seconds from 2.5 seconds.
-Ragnis Locust:
	Regen ammo removed
	Tag added: STRIKE, USE_VS_FRIGATES
	Remove flux cost
-Lili Squall:  
	Remove flux cost
Ship:
-Mazo:
	The two back composite mount are replaced per ballistic mount.
-Heart-Z Mk2:
	Remove the Exp Mk2 per a another Log Mk2.
-Exp Wing:
	Reduce engagement range to 4000 from 4500. 
	Add Lotus shield on the Exp Wing.
	Reduce the armor and hull per 3.
-Log Wing:
	Reduce engagement range to 4000 from 6000. 
	Reduce armor to 70 from 100.
	Reduce hull to 300 form 450.
-Log Mk2:
	Reduce armor to 140 from 200.
	Reduce hull to 500 form 850.
-VoL:
	Reduce OP to 55 from 61.
	Increase Supply maintenance to 7 from 6

BugFixes:
-Fix the station of the native starsystem of Noir faction.
-Fix a crash




Version 1.7unbalanced: Break save game
Modify completely Looters and Noir.
    -Noir get the bubble shield of AI wars mod, but without the failure system.
    -The Noir Bubble shield is better on low flux than high.
    -Looters lost their phase system, they got a damper field, do not work like lattice shield of templar.
    -Looters right click is now a repulse projectile like Exigency or Priwen Burst of Templar. Disabled if your hull has D-mod.
-Remove the Artefact Shield hullmod.
-Ame topspeed increase 20 to 40
-Farfadet topspeed increase 40 to 50
-Modify the Shard gun weapon
-Nerf the Ragnis Locust and Lili Squall damage to 50 from 75.




Version 1.6:
-Fix emp damage of stulav
-Reput armor/hull to normal value for Looters.
-S-Armor reduce the damage of HE taken of 50%


Version 1.53:(fix)
-Remove one plugin(Disable the spawn of chief of looters who is anyways too bugged).
-Add 1 mission for fight the Chief of Looters but you will need to install GS mod.
-This version(1.53) will remove the plugin on your current game. But do not remove the spawn of already spawned Chief of Looters.
-Theses two ships can be recover, now.


Version 1.52:(breakSave)
-Update for be compatible with the next Nexerelin update.
-Modify the enem anti-missile.
-Modify every Looters MK2.
-Fighter MK2 are now undroppable like Terminator.
- Fix the new phase cloak of Looters ships.
-Nerf every damaged ships and modify their sprite with that.
- Increase shield radius of every Noir ships (+15)
- Reduce the flux of the Sting E-Slot per 2.
- Modify sounds of somes weapons
- Nerf topspeed of every Looters ships.
- Remade flag of their  factions.
- Update somes weapons.
-Fix every descriptions  (Made per Gwyvern)
- Nerf to 500 crew The Heart, increase of 300 units cargo and fuel.
- add two tinys frigates

Version 1.51:
-Retire shipsystem for the three Looters fighters, for a best performance.
-Increase topspeed for every Looters fighters for be balanced with the remove of the system.
-Retire shipsystem for the three Noir fighters, for a best performance.
-Increase defense performence for every Noir fighters.(Except (D))
-Each Noir fighters have their omni shield changed to front for a better efficient usage.
-Every Noir Ships have a better arc layout now.
-Put a Phase Cloak on every Looters ships because the previous seems.... useless.
-Increase damage base and flux of Looters weapons:
		TRF1 Cannon Damage: 125 -> 135     Flux: 136 -> 150
		U-5TS Molot Damage: 150 -> 160	   Flux: 111 -> 130
-Modify name of weapons.
-Modify a large Noir Weapon.(These blu)
-Modify a medium Noir Weapon. (The spreagun)


Version 1.5:
-Break save.
-If you play with Nexerelin, for the moment, you need to copy the Artefact\data\config\exerelinFactionConfig folder on the Nexerelin folder for replace the correct variant.
-Merge with the Looters mod.
-Reduce the spawn of my faction ship on pirate and scavanger.
-Merge these two starsystem to a unique one.


Version 1.42(hotfix):
-Fix the variant of soul(D)
-Add a zigzag effect on the PD

Version 1.41(fix):
-Fix the built-in weapon of the Ame capital who never use the weapon.


Version 1.4(Balance part 3):
-Reduce drastrically the number of projectile of every weapon.
-Increase proportionnaly the damage.
-Fix text.


Version 1.35(Balance part 2):
-Remove the slow beam (Sorry but too useless for the Ame!)
-Add a Huge Artefact weapon(N) (who replace the slow beam)


Version 1.3(Balance):
-Increase OP for every ship: (They have problematic lack of OP) (Bonus is the "tech" of the ship, a freighter have less and a battle ship have more)
	Fantome 31 -> 40 (Level of a wolf)
	Ame 318 -> 370 (Maintenance cost + Carrier, he have 0 bonus before, now he have the same than the Legion)
	Farfadet 60 -> 80 (Level of weak cruiser)
	Ghost 135 -> 155 (Level of aurora)
	Ombre 66 -> 80  (15 bonus)
	Soul 60 -> 65  (0 bonus)
-All variants have be modified for have the new OP stats.
-Variants of the Soul have be modified for be more freighter, he have vanilla mining weapon on variant, now.
-Weapons OP have not be modified because they cost a balance cost.


Version 1.24(fix):
-Fix description.
-Rebalance the (R) weapon

Version 1.23(fix):
-Balance (R) Weapon for big fleet. Modify many things on the missile.

Version 1.22:
-Fix Story of Farfadet.
-Fix Story of Fantome and Ombre.


Version 1.21:(fix)
-Fix bugged AI of the PD Artefact light cannon.  Less efficient with that, but always very good.
-Fix effect of the Artefact shotgun.


Version 1.2:
-Break the save.
-Reduce the freighter capacity of the Farfadet (Too big)   (Cargo 410 -> 200) (fuel 530 -> 250)
-Change the story.
-Change the relation With Looters.
-Change the system where they live.
-Add vanilla Freighter, tanker, for have "better" market.
-Add hullmods sell per the market. 
-They hate Luddic to max.
-Delete Artefact fleet on luddic path.
-Reduce size of the sprite of two fighters.
-Add a black sub-color to Artefact.


Version 1.173: Fix.
-Fix homing of the Artefact Large Shotgun (B), sorry


Version 1.172: (Rebalance)
-Projectiles of Artefact medium spreagun (B) do not hit friendly, now.
-Change the Artefact small and medium cannon (N) for be more usable.
-Balance the flux to normal with vanilla ship. (But their flux capacity/dissipation are on superior high-tech)
-Weapon flux have be rebalanced for that, also.
-Reduce the freighter capacity of Ombre. (Cargo 190-> 160) (Fuel 190 -> 130)
-The Ghost per default is the unique fighter ship of Noir faction, so they have a better shield, 45� arc  => 75�
-Fix Crash with the slow beam.

Version 1.171:
-Fix descriptions.
-Modify Spectre, change the middle weapon than the spectre never use per a small version PD of the Artefact small PD(B)
-Clean up the Artefact small PD(B), change EMP damage per just damage, less power than PD burst laser. Range 1000 to 500, but change very nothing. 
-Wraith have be renamed to Idolon, do not put a problem with your save.


Version 1.17:
-Clean up the everyframe plugin who cost to your computer. He cost nothing now. (Except if you use the  Slow beam on a combat, else nothing.)
-Fix typos.
-Increase Large blu weapon (the shotgun). 100->140 per projectile


Version 1.16:
-Fix many descriptions.
-Qwertronix have recreate annihilate weapon description.
-Change the OP of annihilate weapon to 16-> 28.
-The Ghost cruiser OP : 95->135
-Fix Farfadet problem with the ring.
-Improve graphical effect of all weapon.
-Modify the Shotgun and Apocalypse cannon(Better). 
-Weapon type modified on energy because this is more logic.
-Add two new ships system.  One for carrier ship and the other for support ship.
-Like some weapon is now energy, damage of (R) weapon is reduced.
-Corvus mode available for Nexerelin on the next update of nexerelin.
-The 1.2  correspond to all descriptions finished.
This update can be followed per fix.
This update is not incompatible with your save.

Version 1.15:
-Fix 2 crashs.
-Upgrade the Sting weapon of the two ships(D)
-Increase life of Wings.


Version 1.14:
-Nerfed Soul flux: max flux 5000-> 2500. Flux dissipation 350 -> 250 This is a freighter with a launch bay, not a fighter.
-Add three Ships(D)


Version 1.13:
-Fix "ugly" effect on each ship.
-Increase crew requirement for wings.
	Spectre 1 -> 4
	Shadow 3 -> 6
-Ame : Maintenance cost doubled : 30->60, all others Artefact ship have low maintenance, but Ame is a heavy aircraft,freighter, battle ship.

Version 1.123:
-Add stability on star system Nebula.
-Add a planet, retire the market of the station.
-Fix description, finally.

For balanced value with the vanilla, value have be increased.
-Wings have see  the refit per two.  They do not dead very often because their are support and they have a big size.
-Wraith OP cost: 3-> 6
-Spectre OP cost: 6-> 9
-Ombre OP : 60->66
-Soul OP : 57->60
-Ame OP unchanged
-Impact of many weapon have be reduced to be balanced with value of vanilla weapons.

Version 1.122:
-Fix crash classexception.


Version 1.121:
-Fix many stats no balanced of each ship. More efficient.



Version 1.12:
This is the time for dead!
Not seriously, i have missed the PD, this is not a flare seeker.
-Fix the power of blu PD to null. This is a homing missile good only for focus qew missiles

Version 1.11:
-Fix crash with a hullmod.
-Fix string of fighter

Version 1.10:
-Change all sprites.
-Change (R) Weapon to HIGH_EXPLOSIVE
-Change (B) medium and large to KINETIC(MIRV Projectile also)
-Tesla Weapon completely modified.
-Fix hull repair.
-(R) Weapon target now correctly when you set a target.
-(N) medium and small are now guidedPOOR also
- Increase the power of the Fantome.
- (B) PD small, have be modified.
- Lightning Weapon have be modified for a very power PD burst laser.
- Leecher beam have be nerfed to override beam. (Just do not decrease your flux, anymore)
- All fighters have be modified:
	-Wraith is a PD support
	-Shadow is a powerfull fighter with 3 override beam.
	-Spectre is a powerfull sniper support.


Version 1.042:
-Compatible Nexerelin (Nexerelin is not compatible with for the moment, but you can put file for compatible)

Version 1.041:
-Fix case typos for Linux

Version 1.04:
-Increase the price of the Farfadet  (x2.2) (the shipsystem is very too good)
-Increase the tier of each weapon

Version 1.03:
-Change sprite projectile.
-Change HStulard and ABL weapons, go try!
-Add version checker.
-Nerf flux regen of multiples ships
-Fix error with the hull regen.
-Increase collision radius of all ships.
-Nerf arc of the large weapon of the ghost


Version 1.02:
-Fix bug with version checker( Compatible but do not include version checker)


Version 1.01:
-Delete illegal stuff: Random mission fleet.
-Nerf flight time of all R weapons and N weapon.
-Change the limit of repair hull, now based on the current CR. If life below 30%, repair always.
-Fix fleet generation
-Nerf max flux.

Version 1.00:
-UPdated to 0.8a
-Remove many weapon do not work anymore

