package data.missions.Art_SimpleMission;

import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;

public class MissionDefinition implements MissionDefinitionPlugin {

    @Override
    public void defineMission(MissionDefinitionAPI api) {

        // Set up the fleets
        api.initFleet(FleetSide.PLAYER, "ISS", FleetGoal.ATTACK, false);
        api.initFleet(FleetSide.ENEMY, "ISS", FleetGoal.ATTACK, true);

        // Set a blurb for each fleet
        api.setFleetTagline(FleetSide.PLAYER, "Mercenary team");
        api.setFleetTagline(FleetSide.ENEMY, "Looters");

        // These show up as items in the bulleted list under 
        // "Tactical Objectives" on the mission detail screen
        api.addBriefingItem("Kill their fleet");

        // Set up the player's fleet
        api.addToFleet(FleetSide.PLAYER, "hound_d_Standard", FleetMemberType.SHIP, "Cherenkov Bloom", false);
        api.addToFleet(FleetSide.PLAYER, "wolf_d_Attack", FleetMemberType.SHIP, "Stranger II", true);
        api.addToFleet(FleetSide.PLAYER, "lasher_Standard", FleetMemberType.SHIP, "Milk Run", false);
        api.addToFleet(FleetSide.PLAYER, "tempest_Attack", FleetMemberType.SHIP, "Cherenkov Bloom", false);
        api.addToFleet(FleetSide.PLAYER, "hyperion_Attack", FleetMemberType.SHIP, "Stranger II", true);
        api.addToFleet(FleetSide.PLAYER, "wolf_Assault", FleetMemberType.SHIP, "Milk Run", false);

        
        api.addToFleet(FleetSide.ENEMY, "Art_VoL_Balanced", FleetMemberType.SHIP, "Balanced", true);
        api.addToFleet(FleetSide.ENEMY, "Art_Boli_Immune", FleetMemberType.SHIP, "Immune", true);
        api.addToFleet(FleetSide.ENEMY, "Art_Mazo_Missile", FleetMemberType.SHIP, "Sniper", true);
        api.addToFleet(FleetSide.ENEMY, "Art_DevL_Balanced", FleetMemberType.SHIP, "Balanced", true);
        api.addToFleet(FleetSide.ENEMY, "Art_DevL_Death", FleetMemberType.SHIP, "Death", true);

        // Set up the map.
        float width = 12000f;
        float height = 12000f;
        float minX = -width / 2;
        float minY = -height / 2;
        api.initMap(minX, -1f * minX, minY, -1f * minY);
    }

}
