package data.missions.Art_noir_vs_vanilla;


import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;
import com.fs.starfarer.api.impl.campaign.ids.Personalities;

public class MissionDefinition implements MissionDefinitionPlugin {
        @Override
	public void defineMission(MissionDefinitionAPI api) {

		// Set up the fleets
		api.initFleet(FleetSide.PLAYER, "ISS", FleetGoal.ATTACK, false);
		api.initFleet(FleetSide.ENEMY, "ISS", FleetGoal.ATTACK, true);

		// Set a blurb for each fleet
		api.setFleetTagline(FleetSide.PLAYER, "Noir");
		api.setFleetTagline(FleetSide.ENEMY, "???");
		
		// These show up as items in the bulleted list under 
		// "Tactical Objectives" on the mission detail screen
		api.addBriefingItem("If you win, send me a screenshoot.");

		
		// Set up the player's fleet
                api.addToFleet(FleetSide.PLAYER, "Art_Soul_Soul", FleetMemberType.SHIP, "Unknow ship", true).getCaptain().setPersonality(Personalities.CAUTIOUS);
                api.addToFleet(FleetSide.PLAYER, "Art_Soul_Saul", FleetMemberType.SHIP, "Unknow ship", true).getCaptain().setPersonality(Personalities.CAUTIOUS);
		api.addToFleet(FleetSide.PLAYER, "Art_Meiying_Attack", FleetMemberType.SHIP, "Unknow ship", true).getCaptain().setPersonality(Personalities.CAUTIOUS);
                api.addToFleet(FleetSide.PLAYER, "Art_Meiying_Defense", FleetMemberType.SHIP, "Unknow ship", true).getCaptain().setPersonality(Personalities.CAUTIOUS);

                api.addToFleet(FleetSide.PLAYER, "Art_Ghost_Ghost", FleetMemberType.SHIP, "Unknow ship", true).getCaptain().setPersonality(Personalities.CAUTIOUS);
                api.addToFleet(FleetSide.PLAYER, "Art_Ghost_Ghast", FleetMemberType.SHIP, "Unknow ship", true).getCaptain().setPersonality(Personalities.CAUTIOUS);
                api.addToFleet(FleetSide.PLAYER, "Art_Ghost_Ghest", FleetMemberType.SHIP, "Unknow ship", true).getCaptain().setPersonality(Personalities.CAUTIOUS);
                api.addToFleet(FleetSide.PLAYER, "Art_Fantome_Fantome", FleetMemberType.SHIP, "Unknow ship", true).getCaptain().setPersonality(Personalities.CAUTIOUS);
                api.addToFleet(FleetSide.PLAYER, "Art_Fantome_Fantoma", FleetMemberType.SHIP, "Unknow ship", true).getCaptain().setPersonality(Personalities.CAUTIOUS);
                api.addToFleet(FleetSide.PLAYER, "Art_Farfadet_Farfadet", FleetMemberType.SHIP, "Unknow ship", true).getCaptain().setPersonality(Personalities.CAUTIOUS);
                api.addToFleet(FleetSide.PLAYER, "Art_Farfadet_Farfadot", FleetMemberType.SHIP, "Unknow ship", true).getCaptain().setPersonality(Personalities.CAUTIOUS);
                api.addToFleet(FleetSide.PLAYER, "Art_Ombre_Ombre", FleetMemberType.SHIP, "Unknow ship", true).getCaptain().setPersonality(Personalities.CAUTIOUS);
                api.addToFleet(FleetSide.PLAYER, "Art_Ombre_Ambre", FleetMemberType.SHIP, "Unknow ship", true).getCaptain().setPersonality(Personalities.CAUTIOUS);
                api.addToFleet(FleetSide.PLAYER, "Art_Ombre_Ombra", FleetMemberType.SHIP, "Unknow ship", true).getCaptain().setPersonality(Personalities.CAUTIOUS);
                api.addToFleet(FleetSide.PLAYER, "Art_Ame_Ame", FleetMemberType.SHIP, "Unknow ship", true).getCaptain().setPersonality(Personalities.CAUTIOUS);
                api.addToFleet(FleetSide.PLAYER, "Art_Soul_D", FleetMemberType.SHIP, "Unknow ship", true).getCaptain().setPersonality(Personalities.CAUTIOUS);
                api.addToFleet(FleetSide.PLAYER, "Art_Fantome_D", FleetMemberType.SHIP, "Unknow ship", true).getCaptain().setPersonality(Personalities.CAUTIOUS);

//		 Set up the enemy fleet 

                api.addToFleet(FleetSide.ENEMY, "onslaught_Elite", FleetMemberType.SHIP, "HSS Phoenix I", false);
                api.addToFleet(FleetSide.ENEMY, "onslaught_Elite", FleetMemberType.SHIP, "HSS Phoenix II", false);
                api.addToFleet(FleetSide.ENEMY, "onslaught_Elite", FleetMemberType.SHIP, "HSS Phoenix I", false);
                api.addToFleet(FleetSide.ENEMY, "wolf_PD", FleetMemberType.SHIP, "HSS Phoenix II", false).getCaptain().setPersonality(Personalities.TIMID);
                api.addToFleet(FleetSide.ENEMY, "wolf_Assault", FleetMemberType.SHIP, "HSS Phoenix II", false).getCaptain().setPersonality(Personalities.TIMID);
                api.addToFleet(FleetSide.ENEMY, "wolf_d_pirates_Attack", FleetMemberType.SHIP, "HSS Phoenix II", false).getCaptain().setPersonality(Personalities.TIMID);
                api.addToFleet(FleetSide.ENEMY, "wolf_PD", FleetMemberType.SHIP, "HSS Phoenix II", false).getCaptain().setPersonality(Personalities.TIMID);
                api.addToFleet(FleetSide.ENEMY, "wolf_Assault", FleetMemberType.SHIP, "HSS Phoenix II", false).getCaptain().setPersonality(Personalities.TIMID);
                api.addToFleet(FleetSide.ENEMY, "wolf_d_pirates_Attack", FleetMemberType.SHIP, "HSS Phoenix II", false).getCaptain().setPersonality(Personalities.TIMID);
                api.addToFleet(FleetSide.ENEMY, "wolf_PD", FleetMemberType.SHIP, "HSS Phoenix II", false);
                api.addToFleet(FleetSide.ENEMY, "wolf_Assault", FleetMemberType.SHIP, "HSS Phoenix II", false);
                api.addToFleet(FleetSide.ENEMY, "wolf_d_pirates_Attack", FleetMemberType.SHIP, "HSS Phoenix II", false);
                api.addToFleet(FleetSide.ENEMY, "wolf_PD", FleetMemberType.SHIP, "HSS Phoenix II", false);
                api.addToFleet(FleetSide.ENEMY, "wolf_Assault", FleetMemberType.SHIP, "HSS Phoenix II", false);
                api.addToFleet(FleetSide.ENEMY, "wolf_d_pirates_Attack", FleetMemberType.SHIP, "HSS Phoenix II", false);

                // Set up the map.
		float width = 10000f;
		float height = 10000f;
                float minX = -width/2;
		float minY = -height/2;
		api.initMap(minX,-1f*minX,minY,-1f*minY);
		
	}

}






