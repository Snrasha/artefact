package data.missions.Art_lte_station1;


import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;
import com.fs.starfarer.api.impl.campaign.ids.Personalities;

public class MissionDefinition implements MissionDefinitionPlugin {
        @Override
	public void defineMission(MissionDefinitionAPI api) {

		// Set up the fleets
		api.initFleet(FleetSide.PLAYER, "ISS", FleetGoal.ATTACK, false);
		api.initFleet(FleetSide.ENEMY, "ISS", FleetGoal.ATTACK, true);

		// Set a blurb for each fleet
		api.setFleetTagline(FleetSide.PLAYER, "???");
		api.setFleetTagline(FleetSide.ENEMY, "???");
		
		// These show up as items in the bulleted list under 
		// "Tactical Objectives" on the mission detail screen
		api.addBriefingItem("If you win, send me a screenshoot.");

		
		// Set up the player's fleet
                api.addToFleet(FleetSide.PLAYER, "onslaught_xiv_Elite", FleetMemberType.SHIP, "Unknow ship", true).getCaptain().setPersonality(Personalities.CAUTIOUS);
                api.addToFleet(FleetSide.PLAYER, "onslaught_Standard", FleetMemberType.SHIP, "Unknow ship", true).getCaptain().setPersonality(Personalities.CAUTIOUS);
		api.addToFleet(FleetSide.PLAYER, "onslaught_Elite", FleetMemberType.SHIP, "Unknow ship", true).getCaptain().setPersonality(Personalities.CAUTIOUS);
                api.addToFleet(FleetSide.PLAYER, "onslaught_Outdated", FleetMemberType.SHIP, "Unknow ship", true).getCaptain().setPersonality(Personalities.CAUTIOUS);
                
                api.addToFleet(FleetSide.ENEMY, "Art_lte_station1_Standard", FleetMemberType.SHIP, "HSS Phoenix I", false);

                // Set up the map.
		float width = 10000f;
		float height = 10000f;
                float minX = -width/2;
		float minY = -height/2;
		api.initMap(minX,-1f*minX,minY,-1f*minY);
		
	}

}






