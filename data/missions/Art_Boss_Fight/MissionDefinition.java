package data.missions.Art_Boss_Fight;

import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;

public class MissionDefinition implements MissionDefinitionPlugin {

    @Override
    public void defineMission(MissionDefinitionAPI api) {

        // Set up the fleets
        api.initFleet(FleetSide.PLAYER, "ISS", FleetGoal.ATTACK, false);
        api.initFleet(FleetSide.ENEMY, "ISS", FleetGoal.ATTACK, true);

        // Set a blurb for each fleet
        api.setFleetTagline(FleetSide.PLAYER, "Mercenary team");
        api.setFleetTagline(FleetSide.ENEMY, "Looters");

        // These show up as items in the bulleted list under 
        // "Tactical Objectives" on the mission detail screen
        api.addBriefingItem("Kill their fleet");

        // Set up the player's fleet
        api.addToFleet(FleetSide.PLAYER, "paragon_Escort", FleetMemberType.SHIP, "TTS Invincible", true);
        api.addToFleet(FleetSide.PLAYER, "hyperion_Strike", FleetMemberType.SHIP, "TTS Chimera", true);
        api.addToFleet(FleetSide.PLAYER, "tempest_Attack", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "tempest_Attack", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "gemini_Standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "conquest_Standard", FleetMemberType.SHIP, "MSS Garuda", true);
        api.addToFleet(FleetSide.PLAYER, "eagle_Assault", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "falcon_CS", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "falcon_CS", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "heron_Strike", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "heron_Strike", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "hammerhead_Balanced", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "sunder_CS", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "gemini_Standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "vigilance_FS", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "vigilance_FS", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "vigilance_FS", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "vigilance_FS", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "vigilance_FS", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.PLAYER, "vigilance_FS", FleetMemberType.SHIP, false);

        api.addToFleet(FleetSide.ENEMY, "Art_Heart-Z-Mk2_Boss", FleetMemberType.SHIP, "Boss", true);
        api.addToFleet(FleetSide.ENEMY, "Art_M12-II-Mk2_Strike", FleetMemberType.SHIP, "Strike", true);
        api.addToFleet(FleetSide.ENEMY, "Art_M12-II-Mk2_Strike", FleetMemberType.SHIP, "Strike", true);
        api.addToFleet(FleetSide.ENEMY, "Art_P5-V_Balanced", FleetMemberType.SHIP, "Balanced", true);
        api.addToFleet(FleetSide.ENEMY, "Art_P5-V_Immune", FleetMemberType.SHIP, "Immune", true);
        api.addToFleet(FleetSide.ENEMY, "Art_P5-V_Offensive", FleetMemberType.SHIP, "Immune", true);

        api.addToFleet(FleetSide.ENEMY, "Art_VoL_Death", FleetMemberType.SHIP, "Death", true);
        api.addToFleet(FleetSide.ENEMY, "Art_VoL_Death", FleetMemberType.SHIP, "Death", true);
        api.addToFleet(FleetSide.ENEMY, "Art_VoL_Death", FleetMemberType.SHIP, "Death", true);
        api.addToFleet(FleetSide.ENEMY, "Art_VoL_Death", FleetMemberType.SHIP, "Death", true);

        
// Set up the map.
        float width = 12000f;
        float height = 12000f;
        float minX = -width / 2;
        float minY = -height / 2;
        api.initMap(minX, -1f * minX, minY, -1f * minY);
    }

}
