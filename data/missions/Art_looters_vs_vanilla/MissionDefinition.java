package data.missions.Art_looters_vs_vanilla;

import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.fleet.FleetGoal;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.mission.FleetSide;
import com.fs.starfarer.api.mission.MissionDefinitionAPI;
import com.fs.starfarer.api.mission.MissionDefinitionPlugin;
import java.util.List;

public class MissionDefinition implements MissionDefinitionPlugin {

    @Override
    public void defineMission(MissionDefinitionAPI api) {

        // Set up the fleets
        api.initFleet(FleetSide.PLAYER, "ISS", FleetGoal.ATTACK, false);
        api.initFleet(FleetSide.ENEMY, "ISS", FleetGoal.ATTACK, true);

        // Set a blurb for each fleet
        api.setFleetTagline(FleetSide.PLAYER, "Looters");
        api.setFleetTagline(FleetSide.ENEMY, "Ennemi");

        // These show up as items in the bulleted list under 
        // "Tactical Objectives" on the mission detail screen
        api.addBriefingItem("Just win");

        // Set up the player's fleet

        api.addToFleet(FleetSide.PLAYER, "Art_P5-V_Immune", FleetMemberType.SHIP, "Immune", true);
        api.addToFleet(FleetSide.PLAYER, "Art_VoL_Death", FleetMemberType.SHIP, "Death", true);
        api.addToFleet(FleetSide.PLAYER, "Art_Mazo_Missile", FleetMemberType.SHIP, "Death", true);
        api.addToFleet(FleetSide.PLAYER, "Art_Boli_Sniper", FleetMemberType.SHIP, "Sniper", true);
        api.addToFleet(FleetSide.PLAYER, "Art_DevL_Balanced", FleetMemberType.SHIP, "Balanced", true);
        api.addToFleet(FleetSide.PLAYER, "Art_M12-II_Sniper", FleetMemberType.SHIP, "Sniper", true);
        api.addToFleet(FleetSide.PLAYER, "Art_M6-X_Standard", FleetMemberType.SHIP, "Balanced", true);
        api.addToFleet(FleetSide.PLAYER, "Art_PiL_Standard", FleetMemberType.SHIP, "Balanced", true);
        api.addToFleet(FleetSide.PLAYER, "Art_P7-L_Standard", FleetMemberType.SHIP, "Balanced", true);
        api.addToFleet(FleetSide.PLAYER, "Art_Heart-Z_Offensive", FleetMemberType.SHIP, "Balanced", true);
        api.addToFleet(FleetSide.PLAYER, "Art_P5-V_D", FleetMemberType.SHIP, "Immune", true);
        api.addToFleet(FleetSide.PLAYER, "Art_VoL_D", FleetMemberType.SHIP, "Death", true);
        api.addToFleet(FleetSide.PLAYER, "Art_Boli_D", FleetMemberType.SHIP, "Immune", true);
        api.addToFleet(FleetSide.PLAYER, "Art_DevL_D", FleetMemberType.SHIP, "Balanced", true);
        api.addToFleet(FleetSide.PLAYER, "Art_M12-II_D", FleetMemberType.SHIP, "Sniper", true);
        api.addToFleet(FleetSide.PLAYER, "Art_Heart-Z_D", FleetMemberType.SHIP, "Balanced", true);

    
        // Set up the enemy fleet
        api.addToFleet(FleetSide.ENEMY, "onslaught_Elite", FleetMemberType.SHIP, "HSS Phoenix I", false);
        api.addToFleet(FleetSide.ENEMY, "onslaught_xiv_Elite", FleetMemberType.SHIP, "HSS Phoenix II", false);
        api.addToFleet(FleetSide.ENEMY, "condor_Attack", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "condor_Support", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "condor_Strike", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "gryphon_Standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "enforcer_Assault", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "hound_Standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "hound_Standard", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "lasher_CS", FleetMemberType.SHIP, false);
        api.addToFleet(FleetSide.ENEMY, "lasher_CS", FleetMemberType.SHIP, false);

        float width = 24000f;
        float height = 18000f;
        api.initMap(-width / 2f, width / 2f,  -height / 2f, height / 2f);

        float minX = -width / 2;
        float minY = -height / 2;

        api.addNebula(minX + width * 0.5f - 300, minY + height * 0.5f, 1000);
        api.addNebula(minX + width * 0.5f + 300, minY + height * 0.5f, 1000);

        for (int i = 0; i < 5; i++) {
            float x = (float) Math.random() * width - width / 2;
            float y = (float) Math.random() * height - height / 2;
            float radius = 100f + (float) Math.random() * 400f;
            api.addNebula(x, y, radius);
        }

        // Add an asteroid field
        api.addAsteroidField(minX + width / 2f, minY + height / 2f, 0, 8000f,
                20f, 70f, 100);
/*
        api.addPlugin(new BaseEveryFrameCombatPlugin() {
            public void init(CombatEngineAPI engine) {
                engine.getContext().setStandoffRange(6000f);
            }

            public void advance(float amount, List events) {
            }
        });*/
    }

}
