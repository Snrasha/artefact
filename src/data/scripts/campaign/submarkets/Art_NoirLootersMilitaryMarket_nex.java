package src.data.scripts.campaign.submarkets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import exerelin.campaign.submarkets.Nex_MilitarySubmarketPlugin;


public class Art_NoirLootersMilitaryMarket_nex extends Nex_MilitarySubmarketPlugin{
  private static final float TARIFMULT=1.2f;
	
    @Override
    public float getTariff() {
        RepLevel level = submarket.getFaction().getRelationshipLevel(Global.getSector().getFaction(Factions.PLAYER));
        float mult = 1f;
        switch (level)
        {
            case NEUTRAL:
                mult = 0.9f;
                break;
            case FAVORABLE:
                mult = 0.75f;
                break;
            case WELCOMING:
                mult = 0.65f;
                break;
            case FRIENDLY:
                mult = 0.3f;
                break;
            case COOPERATIVE:
                mult = 0.1f;
                break;
            default:
                mult = 1f;
        }
        return mult*TARIFMULT;
    }
}





