package src.data.scripts.campaign.econ;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.econ.CommodityOnMarketAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.econ.MarketImmigrationModifier;
import com.fs.starfarer.api.impl.campaign.econ.BaseMarketConditionPlugin;
import com.fs.starfarer.api.impl.campaign.ids.Submarkets;
import com.fs.starfarer.api.impl.campaign.population.PopulationComposition;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Misc;
import java.awt.Color;

public class Art_Hatred extends BaseMarketConditionPlugin implements MarketImmigrationModifier {

    @Override
    public void apply(String id) {
        market.addTransientImmigrationModifier(this);

        market.getStability().modifyFlat(id, 10, "Hatred");

        
        market.getAccessibilityMod().modifyFlat(id, -0.25f, "Hatred");
        //market.getAccessibilityMod().modifyFlat(id, -1,"Hatred");
        market.getHazard().modifyFlat(id, -0.5f, "Hatred");
      // market.getTariff().modifyFlat("default_tariff", market.getFaction().getTariffFraction());

      boolean haveNexerelin = Global.getSettings().getModManager().isModEnabled("nexerelin");
      if(!haveNexerelin){
      
        boolean lte = market.getFactionId().equals("Lte");
        boolean noir = market.getFactionId().equals("noir");

        if (lte || noir) {
             if (market.hasSubmarket(Submarkets.SUBMARKET_OPEN)) {
                market.removeSubmarket(Submarkets.SUBMARKET_OPEN);
                market.addSubmarket("Art_open_market");
            }
            if (market.hasSubmarket(Submarkets.GENERIC_MILITARY)) {
                market.removeSubmarket(Submarkets.GENERIC_MILITARY);
                market.addSubmarket("Art_generic_military");

            }
            if (market.hasSubmarket(Submarkets.SUBMARKET_BLACK)) {
                market.removeSubmarket(Submarkets.SUBMARKET_BLACK);
                if(noir)
                market.addSubmarket("Art_black_market");
            }
        }
      }

        //    CommodityOnMarketAPI com = market.getCommodityData(id);
        // market.getStats().getDynamic().getMod(Stats.DEMAND_REDUCTION_MOD).modifyFlat(id, 5);
        //  float pop = market.getPopulation().getWeightValue();
        //  market.getDemand(Commodities.ORGANICS).getDemand().modifyFlat(id, pop * ConditionData.WORLD_UNINHABITABLE_ORGANICS_MULT);
    }

    @Override
    public void modifyIncoming(MarketAPI market, PopulationComposition incoming) {
        incoming.add("noir", 10f);
        incoming.add("Lte", 10f);
        incoming.getWeight().modifyFlat(getModId(), 10, Misc.ucFirst(condition.getName().toLowerCase()));

    }

    @Override
    public void unapply(String id) {
        market.removeTransientImmigrationModifier(this);

        market.getStability().unmodify(id);

        market.getAccessibilityMod().unmodifyFlat(id);
        market.getHazard().unmodifyFlat(id);
        //  market.getStats().getDynamic().getMod(Stats.DEMAND_REDUCTION_MOD).unmodifyFlat(id);

    }

    /*  @Override
       public void advance(float amount) {
           if(market.hasCondition(Conditions.PIRATE_ACTIVITY)){
               market.removeCondition(Conditions.PIRATE_ACTIVITY);
           }
           if(market.hasCondition(Conditions.PATHER_CELLS)){
               market.removeCondition(Conditions.PATHER_CELLS);
           }
           
       }*/

    @Override
    protected void createTooltipAfterDescription(TooltipMakerAPI tooltip, boolean expanded) {
        super.createTooltipAfterDescription(tooltip, expanded);
        Color h = Misc.getHighlightColor();
        float opad = 10f;
        tooltip.addPara("%s stability, %s accessibility, %s hazard rating,%s population growth.",
                opad, h,
                "+" + (int) 10, "-" + (int) 25 + "%", "-" + (int) 50 + "%", "+" + (int) 10 + "%");
    }

    @Override
    public boolean hasCustomTooltip() {
        return true;
    }
}
