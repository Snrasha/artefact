/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.data.scripts.campaign.econ;

import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.impl.campaign.ids.Submarkets;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.Nex_MarketCMD;
import exerelin.campaign.InvasionRound;
import exerelin.utilities.InvasionListener;
import java.util.List;

public class Art_MarketListener implements InvasionListener {

    @Override
    public void reportInvadeLoot(InteractionDialogAPI idapi, MarketAPI mapi, Nex_MarketCMD.TempDataInvasion tdi, CargoAPI capi) {
    }

    @Override
    public void reportInvasionRound(InvasionRound.InvasionRoundResult irr, CampaignFleetAPI cfapi, MarketAPI mapi, float f, float f1) {
    }

    @Override
    public void reportInvasionFinished(CampaignFleetAPI cfapi, FactionAPI fapi, MarketAPI mapi, float f, boolean bln) {
    }

    @Override
    public void reportMarketTransfered(MarketAPI mapi, FactionAPI fapi, FactionAPI fapi1, boolean bln, boolean bln1, List<String> list, float f) {
        boolean lte = fapi.getId().equals("Lte");
        boolean noir = fapi.getId().equals("noir");
        boolean lte2 = fapi1.getId().equals("Lte");
        boolean noir2 = fapi1.getId().equals("noir");
        if ((noir2 || lte2) && !(noir || lte)) {
            mapi.removeCondition("art_hatred");

            if (mapi.hasSubmarket("Art_open_market")) {
                mapi.removeSubmarket("Art_open_market");
                mapi.addSubmarket(Submarkets.SUBMARKET_OPEN);
            }
            if (mapi.hasSubmarket("Art_generic_military_nex")) {
                mapi.removeSubmarket("Art_generic_military_nex");
                mapi.addSubmarket(Submarkets.GENERIC_MILITARY);
            }
            if (mapi.hasSubmarket("Art_black_market") || lte2) {
                mapi.removeSubmarket("Art_black_market");
                mapi.addSubmarket(Submarkets.SUBMARKET_BLACK);
            }
        } else if ((noir || lte) && !(noir2 || lte2)) {
            mapi.addCondition("art_hatred");

            if (mapi.hasSubmarket(Submarkets.SUBMARKET_OPEN)) {
                mapi.removeSubmarket(Submarkets.SUBMARKET_OPEN);
                mapi.addSubmarket("Art_open_market");
            }
            if (mapi.hasSubmarket(Submarkets.GENERIC_MILITARY)) {
                mapi.removeSubmarket(Submarkets.GENERIC_MILITARY);
                mapi.addSubmarket("Art_generic_military_nex");

            }
            if (mapi.hasSubmarket(Submarkets.SUBMARKET_BLACK)) {
                mapi.removeSubmarket(Submarkets.SUBMARKET_BLACK);
                if(noir)
                mapi.addSubmarket("Art_black_market");
            }

        }

    }

}
