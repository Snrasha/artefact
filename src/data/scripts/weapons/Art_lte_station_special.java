package src.data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import java.awt.Color;
import java.util.List;

public class Art_lte_station_special implements EveryFrameWeaponEffectPlugin {

    public float compteur = 10;
    public float compteur2 = 15;
    // private static final Color INNERCOLOR = new Color(200, 75, 200, 200);
    // private static final Color OUTERCOLOR = new Color(255, 100, 255, 255);

    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        if (engine == null) {
            return;
        }
        if (engine.isPaused()) {
            return;
        }
        if (weapon.getShip().isHulk()) {
            return;
        }

        compteur -= amount;
        compteur2 -= amount;
        if (compteur < 0) {
            compteur = 10;
            compteur2 = 5;

            List<ShipAPI> moduleschildcopy = weapon.getShip().getChildModulesCopy();

            for (ShipAPI module : moduleschildcopy) {
                if (module.isAlive()) {
                    Global.getCombatEngine().spawnEmpArc(weapon.getShip(), weapon.getLocation(), weapon.getShip(), module,
                            DamageType.ENERGY,
                            0,
                            0, // emp 
                            300f, // max range 
                            "system_emp_emitter_impact",
                            8, // thickness
                            Color.PINK, Color.WHITE
                    );
                }
            }
        }
        if (compteur2 < 0) {
            compteur2 = 5;

            List<ShipAPI> moduleschildcopy = weapon.getShip().getChildModulesCopy();
            ShipAPI firstModule;
            if (moduleschildcopy.isEmpty()) {
                return;
            } else {
                firstModule = moduleschildcopy.get(0);
            }

            ShipAPI previousModule = null;
            for (ShipAPI module : moduleschildcopy) {

                if (module.isAlive()) {
                    if (previousModule != null) {
                        Global.getCombatEngine().spawnEmpArc(previousModule, previousModule.getLocation(), previousModule, module,
                                DamageType.ENERGY,
                                0,
                                0, // emp 
                                300f, // max range 
                                "system_emp_emitter_impact",
                                8, // thickness
                                Color.YELLOW, Color.WHITE
                        );
                    }
                    previousModule = module;
                }
            }

            Global.getCombatEngine().spawnEmpArc(previousModule, previousModule.getLocation(), previousModule, firstModule,
                    DamageType.ENERGY,
                    0,
                    0, // emp 
                    300f, // max range 
                    "system_emp_emitter_impact",
                    8, // thickness
                    Color.YELLOW, Color.WHITE
            );
        }

    }

}
