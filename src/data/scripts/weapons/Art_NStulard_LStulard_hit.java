package src.data.scripts.weapons;

import java.awt.Color;


import com.fs.starfarer.api.combat.*;
import org.lwjgl.util.vector.Vector2f;

public class Art_NStulard_LStulard_hit implements OnHitEffectPlugin {


    @Override
    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target,
                      Vector2f point, boolean shieldHit, CombatEngineAPI engine) {

        if (target instanceof ShipAPI && !shieldHit) {   //only if hits hull
            do {
                double rnd1 = .5 - Math.random();      //see below for details
                float rn1 = (float)rnd1;
                double rnd2 = .5 - Math.random();
                float rn2 = (float)rnd2;    // +(float)rnd*2 to create a ring outside of ship
                float px = target.getLocation().getX();   //get center of cloud
                float py = target.getLocation().getY();   //get center of cloud
                float sr = target.getCollisionRadius();   //replace to change to manual area or effect
                Vector2f pa = new Vector2f(px + sr*rn1, py + sr*rn2);   //replace "sr" to change to manual area or effect
                float emp = projectile.getEmpAmount();
                float dam = projectile.getDamageAmount();
                engine.spawnEmpArc(projectile.getSource(), pa, target, target,
                    DamageType.ENERGY,
                    dam,
                    emp, // emp
                    sr, // max range prevent bolds hitting wrong side of the ship
                    "tachyon_lance_emp_impact",
                    20f, // thickness
                    new Color(200,255,205,55),
                    new Color(200,255,205,55)
                );
            }while ((float) Math.random() > 0.6f && target instanceof ShipAPI); //duration
        }
        else if (target instanceof ShipAPI && shieldHit) {     //only if hits shield
            do {
                double rnd1 = .5 - Math.random(); //generate a random double with -vs component, range -.5 to +.5
                float rn1 = (float)rnd1;          // convert to float
                double rnd2 = .5 - Math.random();
                float rn2 = (float)rnd2;    // +(float)rnd*2 to create a ring (implemented later in a better way)
                float px = target.getLocation().getX();   //get center of cloud
                float py = target.getLocation().getY();
                float hx = point.getX();   //get center of cloud
                float hy = point.getY();   //get center of cloud
                float sr = target.getCollisionRadius();   //replace to change to manual area or effect
                Vector2f pa = new Vector2f(hx + 1/2*(hx-px) + sr*rn1, hy + 1/2*(hy-py) + sr*rn2);   //cause bolt spawn center to be just outside of ship radius,
                //at the point where it was hit, within a radius bounded by max range(5/3*sr) and (1/2*sr) around point
                float emp = projectile.getEmpAmount();
                float dam = projectile.getDamageAmount();
                engine.spawnEmpArc(projectile.getSource(), pa, target, target,
                        DamageType.ENERGY,
                        2/4*dam, //make it less shield destroying
                        emp, // emp
                        5/3*sr, // max range make bolts hit hull closest
                        "tachyon_lance_emp_impact",
                        20f, // thickness
                        new Color(200,255,205,55),
                        new Color(200,255,205,55)
                );
                if ((float) Math.random() > 0.85f) { //generate additional bolts around ship 15%chance per loop
                    double rnd1b = .25 - .5*Math.random();
                    float rn1b = (float)rnd1b+(float)rnd1b*5;
                    double rnd2b = .25 - .5*Math.random();
                    float rn2b = (float)rnd2b+(float)rnd2b*5;    // +(float)rnd*2 to create a ring outside of ship
                    float pxb = target.getLocation().getX();   //get center of cloud
                    float pyb = target.getLocation().getY();   //get center of cloud
                    Vector2f pab = new Vector2f(pxb + sr*rn1b, pyb + sr*rn2b);   //replace "sr" to change to manual area or effect
                    engine.spawnEmpArc(projectile.getSource(), pab, target, target,
                            DamageType.ENERGY,
                            dam,
                            emp, // emp
                            10000f, // max range
                            "tachyon_lance_emp_impact",
                            20f, // thickness
                            new Color(200,255,205,55),
                            new Color(200,255,205,55)
                    );
                }

            }while ((float) Math.random() > 0.6f && target instanceof ShipAPI); //duration
        }

    }
}

