package src.data.scripts.weapons;

import com.fs.starfarer.api.combat.*;
import java.awt.Color;
import org.lwjgl.util.vector.Vector2f;

public class Art_HStulard_hit implements OnHitEffectPlugin {

    @Override
    public void onHit(DamagingProjectileAPI projectile, CombatEntityAPI target,
            Vector2f point, boolean shieldHit, CombatEngineAPI engine) {

        CombatEntityAPI entity = engine.spawnProjectile(projectile.getSource(), projectile.getWeapon(), "Art_Black_Hole", projectile.getLocation(), projectile.getFacing(), new Vector2f(0, 0));
        if (entity instanceof MissileAPI) {
            MissileAPI missile = (MissileAPI) entity;
            missile.setSource(projectile.getSource());
            missile.setOwner(projectile.getOwner());

            if (missile.getMissileAI() instanceof GuidedMissileAI) {
                GuidedMissileAI c = (GuidedMissileAI) missile.getMissileAI();
                c.setTarget(target);
            }
        }

        Vector2f ZERO = new Vector2f();
        engine.spawnExplosion(point, ZERO, Color.RED, 50, 1.5f);
        engine.spawnExplosion(point, ZERO, Color.BLUE, 150, 1.5f);
        engine.addHitParticle(point, ZERO, 250, 1f, 1.5f, Color.YELLOW);
        engine.addSmoothParticle(point, ZERO, 500, 1f, 1.5f, Color.CYAN);
        engine.addHitParticle(point, ZERO, 300, 1f, 1.5f, Color.PINK);

    }
}
