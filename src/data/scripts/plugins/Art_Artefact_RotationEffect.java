package src.data.scripts.plugins;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.WeaponAPI;

public class Art_Artefact_RotationEffect implements EveryFrameWeaponEffectPlugin {

    private final float degrees_per_second = 4.8f;
    private float angle = 0.0f;

    private int chargeLevel = 0;

    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        if (engine.isPaused()) {
            return;
        }
        if (weapon.getShip().isHulk()) {
            return;
        }
        ShipSystemAPI system = weapon.getShip().getSystem();

        if (system.isChargedown()) {
            if (chargeLevel > 0) {
                chargeLevel -= 0.2;
            }
        } else if (system.isChargeup()) {
            chargeLevel += 1;
        }

        float direction;
        switch (weapon.getSize()) {
            case SMALL:
                direction = -1.0f;
                angle = normalizeAngle(angle + (amount * direction * (degrees_per_second + chargeLevel)));
                break;
            case MEDIUM:
                direction = 1.0f;
                angle = normalizeAngle(angle + (amount * direction * (degrees_per_second + chargeLevel)));
                weapon.getShip().setFacing(-angle);
                break;
            case LARGE:
                direction = 1.0f;
                angle = normalizeAngle(angle + (amount * direction * (degrees_per_second/2)));
                break;
        }
        weapon.setCurrAngle(angle);
    }

    public static float normalizeAngle(float angleDeg) {
        return (angleDeg % 360f + 360f) % 360f;
    }
}
