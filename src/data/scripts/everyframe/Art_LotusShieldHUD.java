package src.data.scripts.everyframe;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.input.InputEventAPI;
import data.scripts.util.MagicUI;
import java.awt.Color;
import java.util.List;
import src.data.hullmods.Art_LotusShield;
import src.data.hullmods.Art_LotusShield.ShieldState;

/**
 * Hud to display, always, and only for player.
 *
 * @author Snrasha
 */
public class Art_LotusShieldHUD extends BaseEveryFrameCombatPlugin {

    @Override
    public void advance(float amount, List<InputEventAPI> events) {
        CombatEngineAPI engine = Global.getCombatEngine();
        if (engine == null) {
            return;
        }
        if (engine.isCombatOver()) {
            return;
        }

        ShipAPI ship = engine.getPlayerShip();

        if (ship != null) {
            Color basecolor = MagicUI.GREENCOLOR;
            
            // If player has a ship with the prefix Art_ (from Artefact mod)
            if (ship.getHullSpec().getBaseHullId().startsWith("Art_")) {
                //If player has a ship with a special HUD.
                ShieldState shieldState = Art_LotusShield.shipShield.get(ship);
                if (shieldState != null) {

                    float fill = shieldState.hitpoint / shieldState.maxhitpoint;
                    //If he has the special HUD, draw also the HUD for the shipsystem.
                    if (ship.getPhaseCloak() != null) {
                        if (ship.getPhaseCloak().getCooldownRemaining() > 0f) {
                            int rounded = Math.round(100 - 100 * (ship.getPhaseCloak().getCooldownRemaining() / ship.getPhaseCloak().getCooldown()));
                            MagicUI.drawSystemBar(ship, Color.RED, (float) (rounded) / 100, 0);
                        } else {
                            MagicUI.drawSystemBar(ship, Color.GREEN, 1, 0);
                        }
                    }
                    //Draw the third bar on the left bottom. Who contains the hitpoint of the shield.
                    MagicUI.drawInterfaceStatusBar(ship, fill, null, null, fill, "shield", (int) shieldState.hitpoint);
                    //Draw the bar
                    MagicUI.drawHUDStatusBar(ship, fill, basecolor, basecolor, fill, "shield", "player", false);
                }
            }
            if (ship.getShipTarget() != null) {
                ShipAPI target = ship.getShipTarget();
                if (target.getHullSpec().getBaseHullId().startsWith("Art_")) {
                    ShieldState shieldState = Art_LotusShield.shipShield.get(target);
                    if (shieldState != null) {

                        if (target.getOwner() != ship.getOwner() && !target.isAlly()) {
                            basecolor = MagicUI.REDCOLOR;
                        }
                        float fill = shieldState.hitpoint / shieldState.maxhitpoint;
                        MagicUI.drawHUDStatusBar(ship, fill, basecolor, basecolor, fill, "shield", "target", true);
                    }
                }
            }
        }

    }
}
