package src.data.scripts.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.GuidedMissileAI;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import com.fs.starfarer.api.util.IntervalUtil;
import java.awt.Color;

import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import org.lazywizard.lazylib.CollectionUtils;
import org.lazywizard.lazylib.FastTrig;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;
// This custom: Basic missile with beautiful effect.

public class Art_Stulard_ai extends Art_MissileStuff implements GuidedMissileAI {
    private final int fakeHitpoint=1000;

    private static final float SHIT_ACCURACY_FACTOR = 0.75f;
    private final IntervalUtil courseCorrectInterval = new IntervalUtil(0.1f, 0.1f);
    private final MissileAPI missile;
    private CombatEntityAPI target;
    private float timer = 0, check;

    public Art_Stulard_ai(MissileAPI missile, ShipAPI launchingShip, float empattack) {
        this.check=empattack;
        this.missile = missile;
        courseCorrectInterval.forceCurrInterval(MathUtils.getRandomNumberInRange(0.1f, 0.6f));

        if (launchingShip.getShipTarget() != null && !launchingShip.getShipTarget().isHulk() && launchingShip.getShipTarget().isAlly() != launchingShip.isAlly()) {
            target = launchingShip.getShipTarget();
        }

        if (target == null) {
            List<ShipAPI> directTargets = CombatUtils.getShipsWithinRange(launchingShip.getMouseTarget(), 200f);
            if (!directTargets.isEmpty()) {
                Collections.sort(directTargets, new CollectionUtils.SortEntitiesByDistance(launchingShip.getMouseTarget()));
                ListIterator<ShipAPI> iter = directTargets.listIterator();
                while (iter.hasNext()) {
                    ShipAPI tmp = iter.next();
                    if (!tmp.isHulk() && tmp.getOwner() != launchingShip.getOwner() && !tmp.isDrone() && !tmp.isFighter()) {
                        setTarget(tmp);
                        break;
                    }
                }
            }
        }

        if (target == null) {
            setTarget(findBestTarget(missile));
        }
    }

    @Override
    public void advance(float amount) {
        if (missile.isFizzling() || !missile.isArmed() || missile.isFading() ||  (((missile.getMaxHitpoints()-fakeHitpoint)/2)>missile.getHitpoints()-fakeHitpoint)) {

            Vector2f zero = new Vector2f();
            Global.getCombatEngine().addSmoothParticle(missile.getLocation(), zero, 150, 0.5f, 1, Color.GRAY);
            Global.getCombatEngine().addSmoothParticle(missile.getLocation(), zero, 50, 0.5f, 1, Color.WHITE);

            Global.getCombatEngine().removeEntity(missile);

            return;
        }

        missile.giveCommand(ShipCommand.ACCELERATE);

        if (target == null || (target instanceof ShipAPI && (((ShipAPI) target).isHulk())) || (missile.getOwner() == target.getOwner())
                || !Global.getCombatEngine().isEntityInPlay(target)) {
            setTarget(findBestTarget(missile));
            if (target == null) {
                return;
            }
        }

        missile.giveCommand(ShipCommand.ACCELERATE);

        if (target == null || (target instanceof ShipAPI && (((ShipAPI) target).isHulk())) || (missile.getOwner() == target.getOwner())
                || !Global.getCombatEngine().isEntityInPlay(target)) {
            setTarget(findBestTarget(missile));
            if (target == null) {
                return;
            }
        }

        courseCorrectInterval.advance(amount);

        if (courseCorrectInterval.intervalElapsed()) {

            float distance = MathUtils.getDistance(target.getLocation(), missile.getLocation());

            if (distance < (300 + target.getCollisionRadius())) {
                for (int i = 0; i < 3; i++) {
                    Global.getCombatEngine().spawnEmpArc(missile.getSource(), missile.getLocation(), missile, target, DamageType.ENERGY, missile.getDamageAmount() / 2, missile.getEmpAmount(), 500, null, 5, Color.WHITE, Color.BLACK);
                }
                Global.getSoundPlayer().playSound("guardian_pd_fire", 1f, 1f, missile.getLocation(), new Vector2f());
                Vector2f zero = new Vector2f();
                Global.getCombatEngine().addSmoothParticle(missile.getLocation(), zero, 150, 0.5f, 1, Color.GRAY);
                Global.getCombatEngine().addSmoothParticle(missile.getLocation(), zero, 50, 0.5f, 1, Color.WHITE);
                Global.getCombatEngine().removeEntity(missile);
                return;
            }
            timer+=1;
            if (timer >= check) {
                timer = 0;

                ShipAPI ship = AIUtils.getNearestEnemy(missile);
                if (ship != null && MathUtils.getDistance(ship.getLocation(), missile.getLocation()) < 500) {
                    Global.getCombatEngine().spawnEmpArc(missile.getSource(), missile.getLocation(), missile, ship, DamageType.ENERGY, missile.getDamageAmount() / 2, missile.getEmpAmount(), 500, null, 1, Color.WHITE, Color.BLACK);
                }
            }

            float guidance = 0.75f;
            if (missile.getSource() != null) {
                guidance += Math.min(missile.getSource().getMutableStats().getMissileGuidance().getModifiedValue()
                        - missile.getSource().getMutableStats().getMissileGuidance().getBaseValue(), 1f) * 0.25f;
            }
            Vector2f guidedTarget = intercept(missile.getLocation(), missile.getVelocity().length(), target.getLocation(), target.getVelocity());
            if (guidedTarget == null) {
                Vector2f projection = new Vector2f(target.getVelocity());
                float scalar = distance / (missile.getVelocity().length() + 1f);
                projection.scale(scalar);
                guidedTarget = Vector2f.add(target.getLocation(), projection, null);
            }
            Vector2f.sub(guidedTarget, target.getLocation(), guidedTarget);
            guidedTarget.scale(guidance);
            Vector2f.add(guidedTarget, target.getLocation(), guidedTarget);

            float offset = target.getCollisionRadius() * SHIT_ACCURACY_FACTOR * (float) FastTrig.sin(MathUtils.getRandomNumberInRange((float) -Math.PI,
                    (float) Math.PI));
            guidedTarget = MathUtils.getPointOnCircumference(guidedTarget, offset, (float) Math.random() * 360f);

            float courseCorrectingAngle = MathUtils.clampAngle(VectorUtils.getAngle(missile.getLocation(), guidedTarget)
                    + MathUtils.getRandomNumberInRange(-20f, 20f));

            missile.setFacing(courseCorrectingAngle);
        }

    }

    @Override
    public CombatEntityAPI getTarget() {
        return target;
    }

    @Override
    public final void setTarget(CombatEntityAPI target) {
        this.target = target;
    }

}
