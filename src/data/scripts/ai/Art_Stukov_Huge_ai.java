package src.data.scripts.ai;

import com.fs.starfarer.api.Global;

import com.fs.starfarer.api.combat.MissileAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import java.awt.Color;
import org.lwjgl.util.vector.Vector2f;




public class Art_Stukov_Huge_ai implements MissileAIPlugin {
    private final int fakeHitpoint=1000;

    private final MissileAPI missile;

    public Art_Stukov_Huge_ai(MissileAPI missile) {
        this.missile = missile;
        
        
    }

    @Override
    public void advance(float amount) {
         if (missile.isFizzling() || !missile.isArmed() || missile.isFading()|| (((missile.getMaxHitpoints()-fakeHitpoint)/2)>missile.getHitpoints()-fakeHitpoint)) {
                                  
            Vector2f zero = new Vector2f();
            Global.getCombatEngine().addSmoothParticle(missile.getLocation(), zero, 150, 0.5f, 1, Color.cyan);
            Global.getCombatEngine().addSmoothParticle(missile.getLocation(), zero, 50, 0.5f, 1, Color.WHITE);
            
            Global.getCombatEngine().removeEntity(missile);
            return;
        }

        missile.giveCommand(ShipCommand.ACCELERATE);
    }

}
