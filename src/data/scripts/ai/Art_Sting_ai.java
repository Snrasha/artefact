package src.data.scripts.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.GuidedMissileAI;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import java.awt.Color;

import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import org.lazywizard.lazylib.CollectionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;

public class Art_Sting_ai extends Art_MissileStuff implements GuidedMissileAI {

    private final MissileAPI missile;
    private CombatEntityAPI target;
    private float timeout = 1f;

    public Art_Sting_ai(MissileAPI missile, ShipAPI launchingShip) {

        this.missile = missile;

        if (launchingShip.getShipTarget() != null && !launchingShip.getShipTarget().isHulk() && launchingShip.getShipTarget().isAlly() != launchingShip.isAlly()) {
            target = launchingShip.getShipTarget();
        }

        if (target == null) {
            List<ShipAPI> directTargets = CombatUtils.getShipsWithinRange(launchingShip.getMouseTarget(), 200f);
            if (!directTargets.isEmpty()) {
                Collections.sort(directTargets, new CollectionUtils.SortEntitiesByDistance(launchingShip.getMouseTarget()));
                ListIterator<ShipAPI> iter = directTargets.listIterator();
                while (iter.hasNext()) {
                    ShipAPI tmp = iter.next();
                    if (!tmp.isHulk() && tmp.getOwner() != launchingShip.getOwner() && !tmp.isDrone() && !tmp.isFighter()) {
                        setTarget(tmp);
                        break;
                    }
                }
            }
        }

        if (target == null) {
            setTarget(findBestTarget(missile));
        }
    }

    @Override
    public void advance(float amount) {
        if (missile.isFizzling() || !missile.isArmed() || missile.isFading() || ((missile.getMaxHitpoints() / 2) > missile.getHitpoints())) {
            Vector2f zero = new Vector2f();
            Global.getCombatEngine().addSmoothParticle(missile.getLocation(), zero, 15, 0.5f, 1, Color.DARK_GRAY);
            Global.getCombatEngine().addSmoothParticle(missile.getLocation(), zero, 5, 0.5f, 1, Color.GRAY);
            Global.getCombatEngine().removeEntity(missile);
            return;
        }

        if (timeout > 0) {

            missile.setFlightTime(0);
            timeout -= amount;
            if (target == null || (target instanceof ShipAPI && (((ShipAPI) target).isHulk())) || (missile.getOwner() == target.getOwner())
                    || !Global.getCombatEngine().isEntityInPlay(target)) {
                setTarget(findBestTarget(missile));
                if (target == null) {
                    if (missile.getSource() != null) {
                        missile.setFacing(missile.getSource().getFacing());
                    }

                    return;
                }
            }

            /*Vector2f guidedTarget = intercept(missile.getLocation(), missile.getVelocity().length(), target.getLocation(), target.getVelocity());
            if (guidedTarget == null) {
                return;
            }
            float courseCorrectingAngle = MathUtils.clampAngle(VectorUtils.getAngle(missile.getLocation(), guidedTarget)
                    + MathUtils.getRandomNumberInRange(-20f, 20f));
             */
            float courseCorrectingAngle = MathUtils.clampAngle(VectorUtils.getAngle(missile.getLocation(), target.getLocation())
                    + MathUtils.getRandomNumberInRange(-10f, 10));
            missile.setFacing(courseCorrectingAngle);

        } else {

            missile.giveCommand(ShipCommand.ACCELERATE);

        }

    }

    @Override
    public CombatEntityAPI getTarget() {
        return target;
    }

    @Override
    public final void setTarget(CombatEntityAPI target) {
        this.target = target;
    }

}
