/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.data.scripts.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.MissileAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import java.awt.Color;
import java.util.Iterator;
import java.util.List;
import org.lazywizard.lazylib.CollisionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

public class Art_Black_Hole_ai implements MissileAIPlugin {

    private final MissileAPI missile;
    private final MissileAIPlugin missileAI;
    private final IntervalUtil intervalhit;

    public Art_Black_Hole_ai(MissileAPI missile, MissileAIPlugin missileAI, ShipAPI launchingShip) {
        this.missile = missile;
        this.missileAI = missileAI;
        this.missile.setFromMissile(true);
        this.intervalhit = new IntervalUtil(0.1f, 0.1f);
    }

    @Override
    public void advance(float amount) {
        if (Global.getCombatEngine().isPaused()) {
            return;
        }

        this.missileAI.advance(amount);

        intervalhit.advance(amount);

        if (!intervalhit.intervalElapsed()) {
            return;
        }

        CombatEngineAPI engine = Global.getCombatEngine();
        List<ShipAPI> ennemi = AIUtils.getNearbyEnemies(this.missile, this.missile.getCollisionRadius() * 4);

        Vector2f loc;
        float damage;
        Vector2f maxradius = new Vector2f(0, 0);
        Vector2f maxradius2 = new Vector2f(0, 0);
        Iterator<ShipAPI> iter = ennemi.iterator();
        ShipAPI target;
        while (iter.hasNext()) {
            target = iter.next();
            if (target == null) {
                continue;
            }
            if (target.getCollisionClass() == CollisionClass.NONE) {
                continue;
            }
            if (target.isPhased()) {
                continue;
            }
            Vector2f.sub(target.getLocation(), this.missile.getLocation(), maxradius);
            maxradius.normalise();
            if (MathUtils.getDistance(missile, target) < (this.missile.getCollisionRadius() * 2)) {
                Vector2f.add(this.missile.getLocation(), new Vector2f(maxradius.getX() * this.missile.getCollisionRadius(), maxradius.getY() * this.missile.getCollisionRadius()), maxradius2);
                if (target.getExactBounds() == null) {
                    loc = this.missile.getLocation();
                } else {
                    loc = CollisionUtils.getCollisionPoint(this.missile.getLocation(), maxradius2, target);
                }
                if (loc == null) {
                    if (CollisionUtils.isPointWithinBounds(this.missile.getLocation(), target)) {
                        loc = this.missile.getLocation();
                    }
                }

                damage = MathUtils.getDistance(this.missile.getLocation(), target.getLocation());
                try {
                    engine.applyDamage(target, loc, damage, DamageType.ENERGY, 5, true, true, this.missile.getSource());
                    engine.addSmoothParticle(loc, new Vector2f(), 50f, 1f, 0.6f, Color.WHITE);
                } catch (Exception e) {
                }
            }

            Vector2f.add(this.missile.getLocation(), new Vector2f(maxradius.getX() * this.missile.getCollisionRadius() * 4, maxradius.getY() * this.missile.getCollisionRadius() * 4), maxradius2);
            if (target.getExactBounds() == null) {
                loc = this.missile.getLocation();
            } else {
                loc = CollisionUtils.getCollisionPoint(this.missile.getLocation(), maxradius2, target);
            }
            if (loc == null) {
                if (CollisionUtils.isPointWithinBounds(this.missile.getLocation(), target)) {
                    loc = this.missile.getLocation();
                }
            }
            damage = 2*MathUtils.getDistance(this.missile.getLocation(), target.getLocation());
            try {
                engine.spawnEmpArc(this.missile.getSource(), this.missile.getLocation(), target, target,
                        DamageType.ENERGY, damage/2, damage,100f, null, 20f, Color.WHITE, Color.BLACK);          
                    engine.addSmoothParticle(loc, new Vector2f(), 50f, 1f, 0.6f, Color.WHITE);
            } catch (Exception e) {
            }

        }

    }

}
