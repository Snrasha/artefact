package src.data.scripts.world;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.PluginPick;
import com.fs.starfarer.api.campaign.CampaignPlugin;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.combat.GuidedMissileAI;
import com.fs.starfarer.api.combat.MissileAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.campaign.ids.Submarkets;
import src.data.scripts.ai.*;
import exerelin.campaign.SectorManager;
import src.data.scripts.campaign.econ.Art_MarketListener;

import src.data.scripts.utils.Art_GraphicLib_utils;

public class Art_artefactModPlugin extends BaseModPlugin {

    private final String IDEnem = "Art_Enem_shot";
    private final String IDSpreadGun = "Art_SpreadGun_shot";
    private final String IDSpreadGunMIRV = "Art_SpreadGun_mirv_shot";

    private final String IDShard = "Art_Shard_shot";
    private final String IDSTING = "Art_Sting_shot";

    private final String IDLStulard = "Art_LStulard_shot";
    private final String IDNStulard = "Art_NStulard_shot";
    private final String IDHStulard_ROCKET = "Art_HStulard_shot";
    private final String IDHStulard_HOLE = "Art_Black_Hole_shot";
    private final String IDLILI = "Art_Lili_shot";
    private final String IDAME = "Art_AmeMissile_shot";

    @Override
    public void onApplicationLoad() {
        boolean hasLazyLib = Global.getSettings().getModManager().isModEnabled("lw_lazylib");
        if (!hasLazyLib) {
            throw new RuntimeException("High tech armada requires LazyLib!"
                    + "\nGet it at http://fractalsoftworks.com/forum/index.php?topic=5444");
        }
        boolean hasMagicLib = Global.getSettings().getModManager().isModEnabled("MagicLib");
        if (!hasMagicLib) {
            throw new RuntimeException("High tech armada requires MagicLib!"
                    + "\nGet it at http://fractalsoftworks.com/forum/index.php?topic=13718");
        }
        boolean hasShaderLib = Global.getSettings().getModManager().isModEnabled("shaderLib");
        if (!hasShaderLib) {
            throw new RuntimeException("High tech armada requires GraphicsLib!"
                    + "\nGet it at http://fractalsoftworks.com/forum/index.php?topic=10982");
        }
        Art_GraphicLib_utils.init();
    }

    @Override
    public void onNewGame() {
        boolean haveNexerelin = Global.getSettings().getModManager().isModEnabled("nexerelin");
        if (!haveNexerelin || SectorManager.getCorvusMode()) {
            new Art_Gen().generate(Global.getSector());
        }
        if (haveNexerelin) {
            Global.getSector().getListenerManager().addListener(new Art_MarketListener());
        }
    }

    @Override
    public void onNewGameAfterEconomyLoad() {
        super.onNewGameAfterEconomyLoad(); //To change body of generated methods, choose Tools | Templates.
        boolean haveNexerelin = Global.getSettings().getModManager().isModEnabled("nexerelin");

        for (MarketAPI market : Global.getSector().getEconomy().getMarketsInGroup(null)) {
            boolean noir = market.getFactionId().equals("noir");
            boolean lte = market.getFactionId().equals("Lte");
            if (noir || lte) {
                if (!market.hasCondition("art_hatred")) {
                    market.addCondition("art_hatred");
                }
                if (market.hasSubmarket(Submarkets.SUBMARKET_OPEN)) {
                    market.removeSubmarket(Submarkets.SUBMARKET_OPEN);
                    market.addSubmarket("Art_open_market");
                }
                if (market.hasSubmarket(Submarkets.GENERIC_MILITARY)) {
                    market.removeSubmarket(Submarkets.GENERIC_MILITARY);
                    if (haveNexerelin) {
                        market.addSubmarket("Art_generic_military_nex");
                    } else {
                        market.addSubmarket("Art_generic_military");
                    }

                }
                if (market.hasSubmarket(Submarkets.SUBMARKET_BLACK)) {
                    market.removeSubmarket(Submarkets.SUBMARKET_BLACK);
                    if (noir) {
                        market.addSubmarket("Art_black_market");
                    }
                }
            }
        }
    }

    @Override
    public void onGameLoad(boolean newGame) {
        boolean haveNexerelin = Global.getSettings().getModManager().isModEnabled("nexerelin");
        if (haveNexerelin) {
            if (!Global.getSector().getListenerManager().hasListenerOfClass(Art_MarketListener.class)) {
                Global.getSector().getListenerManager().addListener(new Art_MarketListener());
            }

        }
        for (MarketAPI market : Global.getSector().getEconomy().getMarketsInGroup(null)) {
            boolean noir = market.getFactionId().equals("noir");
            boolean lte = market.getFactionId().equals("Lte");
            if (noir || lte) {
                if (!market.hasCondition("art_hatred")) {
                    market.addCondition("art_hatred");
                }
                if (market.hasSubmarket(Submarkets.SUBMARKET_OPEN)) {
                    market.removeSubmarket(Submarkets.SUBMARKET_OPEN);
                    market.addSubmarket("Art_open_market");
                }
                if (market.hasSubmarket(Submarkets.GENERIC_MILITARY)) {
                    market.removeSubmarket(Submarkets.GENERIC_MILITARY);
                    if (haveNexerelin) {
                        market.addSubmarket("Art_generic_military_nex");
                    } else {
                        market.addSubmarket("Art_generic_military");
                    }

                }
                if (market.hasSubmarket(Submarkets.SUBMARKET_BLACK)) {
                    market.removeSubmarket(Submarkets.SUBMARKET_BLACK);
                    if (noir) {
                        market.addSubmarket("Art_black_market");
                    }
                }
            }
        }

        super.onGameLoad(newGame); //To change body of generated methods, choose Tools | Templates.
    }

    /*
    @Override
    public void onNewGameAfterProcGen() {
        super.onNewGameAfterProcGen(); //To change body of generated methods, choose Tools | Templates.
        boolean haveNexerelin = Global.getSettings().getModManager().isModEnabled("nexerelin");
        if (haveNexerelin && !SectorManager.getCorvusMode()) {
            Global.getSector().getEconomy().addUpdateListener(
                    new EconomyUpdateListener(){
                @Override
                public void commodityUpdated(String string) {
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public void economyUpdated() {
                    
                   // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public boolean isEconomyListenerExpired() {
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
            
            
            });
        }
        
        
    }*/
    @Override
    public PluginPick<MissileAIPlugin> pickMissileAI(MissileAPI missile, ShipAPI launchingShip) {

        switch (missile.getProjectileSpecId()) {
            case IDShard:
                return new PluginPick<MissileAIPlugin>(new Art_Shard_ai(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SET);
            case IDEnem:
                return new PluginPick<MissileAIPlugin>(new Art_Enem_ai(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SET);
            case IDAME:
                return new PluginPick<MissileAIPlugin>(new Art_AmeMissile_ai(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SET);
            case IDLILI:
            case IDSTING:
                return new PluginPick<MissileAIPlugin>(new Art_Sting_ai(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SET);
            case IDSpreadGun:
                return new PluginPick<MissileAIPlugin>(new Art_SpreadGun_ai(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SET);
            case IDNStulard:
                return new PluginPick<MissileAIPlugin>(new Art_Stulard_ai(missile, launchingShip, 5), CampaignPlugin.PickPriority.MOD_SET);
            case IDLStulard:
                return new PluginPick<MissileAIPlugin>(new Art_Stulard_ai(missile, launchingShip, 10), CampaignPlugin.PickPriority.MOD_SET);
            case IDHStulard_ROCKET:
                return new PluginPick<MissileAIPlugin>(new Art_Stukov_Huge_ai(missile), CampaignPlugin.PickPriority.MOD_SET);
            case IDHStulard_HOLE:
                return new PluginPick<MissileAIPlugin>(new Art_Black_Hole_ai(missile, new Art_Spreadgun_mirv_ai(missile, launchingShip), launchingShip), CampaignPlugin.PickPriority.MOD_SET);
            case IDSpreadGunMIRV:
                return new PluginPick<MissileAIPlugin>(new Art_Spreadgun_mirv_ai(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SET);
            default:
                return null;
        }
    }
}
