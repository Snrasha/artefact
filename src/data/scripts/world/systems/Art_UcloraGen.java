package src.data.scripts.world.systems;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.JumpPointAPI;
import com.fs.starfarer.api.campaign.OrbitAPI;
import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.impl.campaign.ids.Terrain;
import com.fs.starfarer.api.impl.campaign.procgen.StarAge;
import com.fs.starfarer.api.impl.campaign.procgen.StarSystemGenerator.StarSystemType;
import com.fs.starfarer.api.impl.campaign.terrain.MagneticFieldTerrainPlugin;
import com.fs.starfarer.api.impl.campaign.terrain.MagneticFieldTerrainPlugin.MagneticFieldParams;
import com.fs.starfarer.api.impl.campaign.terrain.StarCoronaTerrainPlugin;
import com.fs.starfarer.api.util.Misc;
import java.awt.Color;

public class Art_UcloraGen {

    public void generate(SectorAPI sector) {
    generateOutpost(sector);

        StarSystemAPI system = sector.createStarSystem("Uclora");
        system.getLocation().set(-10000, -17000);

        system.setType(StarSystemType.NEBULA);
        system.setBackgroundTextureFilename("graphics/backgrounds/Art_back.jpg");

        PlanetAPI star = system.initStar("Art_staruclova", "nebula_center_old", 0, 0);

        PlanetAPI ucloraI = system.addPlanet("Art_ucloraI", star, "I-Uclora", "gas_giant", 0, 700, 6000, 400);

        SectorEntityToken relay = system.addCustomEntity("Art_relay", // unique id
                "Noir Relay", // name - if null, defaultName from custom_entities.json will be used
                "comm_relay", // type of object, defined in custom_entities.json
                "noir"); // faction
        relay.setCircularOrbit(star, 120, 1400, 100);

        JumpPointAPI jumpPoint = Global.getFactory().createJumpPoint("Art_jump_I", "Jump Point");
        OrbitAPI orbit = Global.getFactory().createCircularOrbit(ucloraI, 60, 1900, 240);
        jumpPoint.setOrbit(orbit);
        jumpPoint.setRelatedPlanet(ucloraI);
        jumpPoint.setStandardWormholeToHyperspaceVisual();
        system.addEntity(jumpPoint);

        // Barad magnetic field
        SectorEntityToken barad_field = system.addTerrain(Terrain.MAGNETIC_FIELD,
                new MagneticFieldParams(ucloraI.getRadius() + 300f, // terrain effect band width 
                        (ucloraI.getRadius() + 300f) / 2f, // terrain effect middle radius
                        ucloraI, // entity that it's around
                        ucloraI.getRadius() + 100f, // visual band start
                        ucloraI.getRadius() + 100f + 350f, // visual band end
                        new Color(50, 20, 100, 40), // base color
                        0.5f, // probability to spawn aurora sequence, checked once/day when no aurora in progress
                        new Color(140, 100, 235),
                        new Color(180, 110, 210),
                        new Color(150, 140, 190),
                        new Color(140, 190, 210),
                        new Color(90, 200, 170),
                        new Color(65, 230, 160),
                        new Color(20, 220, 70)
                ));
        barad_field.setCircularOrbit(ucloraI, 0, 0, 100);

        system.addRingBand(ucloraI, "misc", "rings_ice0", 256f, 2, Color.white, 256f, 1050, 45, Terrain.RING, null);
        system.addRingBand(ucloraI, "misc", "rings_ice0", 256f, 2, Color.white, 256f, 2500, 90, Terrain.RING, null);

        PlanetAPI ucloraII = system.addPlanet("Art_ucloraII", star, "II-Uclora", "gas_giant", 250, 500, 5500, 240);
        ucloraII.getSpec().setGlowColor(new Color(245, 50, 20, 100));
        ucloraII.getSpec().setUseReverseLightForGlow(true);
        ucloraII.getSpec().setAtmosphereThickness(0.5f);
        ucloraII.getSpec().setCloudRotation(10f);
        ucloraII.getSpec().setPitch(20);
        ucloraII.getSpec().setAtmosphereThicknessMin(80);
        ucloraII.getSpec().setAtmosphereThickness(0.30f);
        ucloraII.getSpec().setAtmosphereColor(new Color(255, 150, 50, 205));

        barad_field = system.addTerrain(Terrain.MAGNETIC_FIELD,
                new MagneticFieldParams(ucloraI.getRadius() + 200f, // terrain effect band width 
                        (ucloraI.getRadius() + 200f) / 2f, // terrain effect middle radius
                        ucloraI, // entity that it's around
                        ucloraI.getRadius() + 50f, // visual band start
                        ucloraI.getRadius() + 50f + 250f, // visual band end
                        new Color(50, 20, 100, 40), // base color
                        0.5f, // probability to spawn aurora sequence, checked once/day when no aurora in progress
                        new Color(140, 100, 235),
                        new Color(180, 110, 210),
                        new Color(150, 140, 190),
                        new Color(140, 190, 210),
                        new Color(90, 200, 170),
                        new Color(65, 230, 160),
                        new Color(20, 220, 70)
                ));
        barad_field.setCircularOrbit(ucloraII, 0, 0, 100);
        system.addAsteroidBelt(ucloraII, 10, 1000, 200, 10, 45, Terrain.ASTEROID_BELT, null);

        system.addRingBand(ucloraII, "misc", "rings_ice0", 256f, 2, Color.white, 256f, 1000, 40, Terrain.RING, null);
        system.addPlanet("Art_ucloraIII", ucloraII, "III-Uclora", "barren", 0, 60, 1000, 40);

        PlanetAPI ucloraIV = system.addPlanet("Art_ucloraIV", star, "IV-Uclora", "cryovolcanic", 130, 600, 14520, 400);
        ucloraIV.getSpec().setGlowTexture(Global.getSettings().getSpriteName("hab_glows", "aurorae"));
        ucloraIV.getSpec().setGlowColor(new Color(255, 50, 240, 245));
        ucloraIV.getSpec().setUseReverseLightForGlow(true);
        ucloraIV.getSpec().setPitch(15f);
        ucloraIV.getSpec().setTilt(40);
        ucloraIV.getSpec().setPlanetColor(new Color(255, 240, 235));
        ucloraIV.applySpecChanges();

        system.addPlanet("Art_ucloraV", ucloraIV, "V-Uclora", "rocky_ice", 130, 100, 820, 50);
        system.addPlanet("Art_ucloraVI", ucloraIV, "VI-Uclora", "barren", 260, 200, 1200, 50);
        system.addRingBand(ucloraIV, "misc", "rings_ice0", 256f, 2, Color.white, 256f, 1000, 40, Terrain.RING, null);

        Misc.addNebulaFromPNG("data/campaign/terrain/Art_nebula.png",
                0, 0, // center of nebula
                system, // location to add to
                "terrain", "nebula_amber",//starsector-core\graphics\terrain  -> take name of nebula
                4, 4, StarAge.OLD); // number of cells in texture        
        Misc.addNebulaFromPNG("data/campaign/terrain/Art_nebula.png",
                0, 0, // center of nebula
                system, // location to add to
                "terrain", "nebula_amber",//starsector-core\graphics\terrain  -> take name of nebula
                130, 130, StarAge.AVERAGE); // number of cells in texture        

        
        PlanetAPI ucloraVII = system.addPlanet("Art_ucloraVII", star, "VII-Uclora", "cryovolcanic", 0, 400, 2500, 400);
 
         relay = system.addCustomEntity("Art_relay", // unique id
                "Looters Relay", // name - if null, defaultName from custom_entities.json will be used
                "comm_relay", // type of object, defined in custom_entities.json
                "Lte"); // faction
        relay.setCircularOrbit(ucloraVII, 120, 1000, 100);

         jumpPoint = Global.getFactory().createJumpPoint("Art_jump_I", "Jump Point");
         orbit = Global.getFactory().createCircularOrbit(star, 60, 0, 240);
        jumpPoint.setOrbit(orbit);
        //jumpPoint.setRelatedPlanet(star);
        jumpPoint.setStandardWormholeToHyperspaceVisual();
        system.addEntity(jumpPoint);

        // Barad magnetic field
         barad_field = system.addTerrain(Terrain.MAGNETIC_FIELD,
                new MagneticFieldTerrainPlugin.MagneticFieldParams(ucloraVII.getRadius() + 300f, // terrain effect band width 
                        (ucloraVII.getRadius() + 300f) / 2f, // terrain effect middle radius
                        ucloraVII, // entity that it's around
                        ucloraVII.getRadius() + 100f, // visual band start
                        ucloraVII.getRadius() + 100f + 350f, // visual band end
                        new Color(50, 20, 100, 40), // base color
                        0.5f, // probability to spawn aurora sequence, checked once/day when no aurora in progress
                        new Color(140, 100, 235),
                        new Color(180, 110, 210),
                        new Color(150, 140, 190),
                        new Color(140, 190, 210),
                        new Color(90, 200, 170),
                        new Color(65, 230, 160),
                        new Color(20, 220, 70)
                ));
        barad_field.setCircularOrbit(ucloraVII, 0, 0, 100);

        
        PlanetAPI ucloraVIII = system.addPlanet("Art_ucloraVIII", star, "VIII-Uclora", "gas_giant", 90, 500, 5500, 400);
system.addRingBand(ucloraVIII, "misc", "rings_ice0", 256f, 2, Color.white, 256f, 1050, 45, Terrain.RING, null);

        //   system.setHyperspaceAnchor(mefleshanII);
        // Barad magnetic field
        barad_field = system.addTerrain(Terrain.MAGNETIC_FIELD,
                new MagneticFieldTerrainPlugin.MagneticFieldParams(ucloraVII.getRadius() + 200f, // terrain effect band width 
                        (ucloraVII.getRadius() + 200f) / 2f, // terrain effect middle radius
                        ucloraVII, // entity that it's around
                        ucloraVII.getRadius() + 50f, // visual band start
                        ucloraVII.getRadius() + 50f + 250f, // visual band end
                        new Color(50, 20, 100, 40), // base color
                        0.5f, // probability to spawn aurora sequence, checked once/day when no aurora in progress
                        new Color(140, 100, 235),
                        new Color(180, 110, 210),
                        new Color(150, 140, 190),
                        new Color(140, 190, 210),
                        new Color(90, 200, 170),
                        new Color(65, 230, 160),
                        new Color(20, 220, 70)
                ));
        barad_field.setCircularOrbit(ucloraVIII, 0, 0, 100);
      /*  system.addAsteroidBelt(mefleshanII, 10, 1000, 200, 10, 45, Terrain.ASTEROID_BELT, null);
*/
        system.addRingBand(ucloraVIII, "misc", "rings_ice0", 256f, 2, Color.white, 256f, 1000, 40, Terrain.RING, null);
        system.addPlanet("Art_ucloraIX", ucloraVIII, "IX-Uclora", "barren", 0, 60, 1000, 40);

        PlanetAPI ucloraX = system.addPlanet("Art_ucloraX", star, "X-Uclora", "cryovolcanic", 130, 300, 3520, 400);
        ucloraX.getSpec().setGlowTexture(Global.getSettings().getSpriteName("hab_glows", "aurorae"));
        ucloraX.getSpec().setGlowColor(new Color(255, 50, 240, 245));
        ucloraX.getSpec().setUseReverseLightForGlow(true);
        ucloraX.getSpec().setPitch(15f);
        ucloraX.getSpec().setTilt(40);
        ucloraX.getSpec().setPlanetColor(new Color(255, 240, 235));
        ucloraX.applySpecChanges();

        system.addPlanet("Art_ucloraXI", ucloraX, "XI-Uclora", "barren", 130, 100, 820, 50);
        system.addPlanet("Art_ucloraXII", ucloraVII, "XII-Uclora", "barren", 260, 200, 1200, 50);
        system.addRingBand(ucloraX, "misc", "rings_ice0", 256f, 2, Color.white, 256f, 1000, 40, Terrain.RING, null);

         Misc.addNebulaFromPNG("data/campaign/terrain/Art_nebula.png",
                0, 0, // center of nebula
                system, // location to add to
                "terrain", "nebula_amber",//starsector-core\graphics\terrain  -> take name of nebula
                4, 4, StarAge.OLD); // number of cells in texture        
       Misc.addNebulaFromPNG("data/campaign/terrain/Art_nebula.png",
                0, 0, // center of nebula
                system, // location to add to
                "terrain", "nebula_amber",//starsector-core\graphics\terrain  -> take name of nebula
                130, 130, StarAge.AVERAGE); // number of cells in texture        

        
        
        
        
        
        
        
        
        star.setSkipForJumpPointAutoGen(true);

        system.removeEntity(star);
        StarCoronaTerrainPlugin coronaPlugin = Misc.getCoronaFor(star);
        if (coronaPlugin != null) {
            system.removeEntity(coronaPlugin.getEntity());
        }

        system.setStar(null);
        system.initNonStarCenter();
        for (SectorEntityToken entity : system.getAllEntities()) {
            if (entity.getOrbitFocus() == star
                    || entity.getOrbitFocus() == system.getCenter()) {
                entity.setOrbit(null);
            }
        }
        system.getCenter().addTag(Tags.AMBIENT_LS);
 system.setStar(star);
        system.autogenerateHyperspaceJumpPoints(true, false);
        //system.setStar(star);

        /*   system.setStar(null);
        system.initNonStarCenter();*/
    }


 
    private void generateOutpost(SectorAPI sector){
                StarSystemAPI system = sector.getStarSystem("Askonia");

        PlanetAPI star = system.getStar();

        PlanetAPI a1 = system.addPlanet("Art_bin", star, "Bin", "arid", 0, 60, 3500, 200);
        a1.getSpec().setGlowTexture(Global.getSettings().getSpriteName("hab_glows", "asharu"));
        a1.getSpec().setGlowColor(new Color(255, 255, 255, 255));
        a1.getSpec().setUseReverseLightForGlow(true);
        a1.applySpecChanges();

        system = sector.getStarSystem("Tyle");

        SectorEntityToken starsecond= system.getEntityById("antillia");

        
        PlanetAPI sing3_1 = system.addPlanet("Art_blow", starsecond, "Blow", "water", 100, 70, 1500, 300);
        system.addPlanet("Art_monus", sing3_1, "Monus", "cryovolcanic", 30, 40,600, 100);
        
    }
}
