package src.data.scripts.world;

import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorGeneratorPlugin;
import com.fs.starfarer.api.impl.campaign.ids.Factions;

import com.fs.starfarer.api.impl.campaign.shared.SharedData;

import java.util.List;
import src.data.scripts.world.systems.Art_UcloraGen;

public class Art_Gen implements SectorGeneratorPlugin {

    @Override
    public void generate(SectorAPI sector) {
        initFactionRelationships(sector);
        SharedData.getData().getPersonBountyEventData().addParticipatingFaction("Lte");
        initFactionRelationships(sector);
		SharedData.getData().getPersonBountyEventData().addParticipatingFaction("noir");
        
        new Art_UcloraGen().generate(sector);
        
        
    }

    private static void initFactionRelationships(SectorAPI sector) { 
      
        FactionAPI noir = sector.getFaction("noir");
        FactionAPI lte = sector.getFaction("Lte");
        

         List<FactionAPI> factionList = sector.getAllFactions();
        factionList.remove(noir);
        factionList.remove(lte);
         
        for (FactionAPI faction : factionList) {
            noir.setRelationship(faction.getId(), RepLevel.SUSPICIOUS);
            lte.setRelationship(faction.getId(), RepLevel.HOSTILE);
        }

        FactionAPI player = sector.getFaction(Factions.PLAYER);
        
   
        lte.setRelationship(player.getId(), RepLevel.NEUTRAL);
        lte.setRelationship(Factions.NEUTRAL, RepLevel.NEUTRAL);
        noir.setRelationship(player.getId(), RepLevel.NEUTRAL);
        noir.setRelationship(Factions.NEUTRAL, RepLevel.NEUTRAL);
        
        lte.setRelationship(noir.getId(), RepLevel.COOPERATIVE);

        
        noir.setRelationship(Factions.LUDDIC_PATH, RepLevel.VENGEFUL);
        noir.setRelationship(Factions.LUDDIC_CHURCH, RepLevel.VENGEFUL);
        lte.setRelationship(Factions.LUDDIC_PATH, RepLevel.VENGEFUL);
        lte.setRelationship(Factions.LUDDIC_CHURCH, RepLevel.VENGEFUL);
    }
}
