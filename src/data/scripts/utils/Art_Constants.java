package src.data.scripts.utils;

import org.lwjgl.util.vector.Vector2f;

public class Art_Constants {
    public static final float TOTAL_REPAIR_FACTOR = 1.0f;
    public static final float TOTAL_CR_LOSS = 0.01f;
    public static final float BASE_REPAIR_EFFICIENCY = 0.8f;
    public static final float REPAIR_TIMEOUT = 1.0f;
    public static final float BASE_REPAIR_RATE = 0.2f;
    public static final float REPAIR_RATE = 0.01f;
    public static final float EFFICIENCY_CURVE_POWER = 2.0f;

    public static final Vector2f ZERO= new Vector2f(0,0);

}