package src.data.scripts.utils;

import com.fs.starfarer.api.combat.ArmorGridAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.fleet.RepairTrackerAPI;

public class Art_ShipArmorData {
    public ArmorGridAPI armorGrid;
    public int armorCellsX;
    public int armorCellsY;
    public float[][] sinceLastDamage;
    public float[][] expectedRepair;
    public float[][] armorCells;
    public float repairEfficiency = 0.8f;
    public float totalRepaired = 0.0f;
    public float totalRepairedLast = 0.0f;
    public float totalCRLost = 0.0f;
    public float totalRepairCapacity;
    public float maxArmor;
    public boolean initialArmorLoaded;
    public boolean CRSynched = false;
    public RepairTrackerAPI RepairTracker;

    public Art_ShipArmorData(ShipAPI ship) {
        this.armorGrid = ship.getArmorGrid();
        
        this.maxArmor = this.armorGrid.getMaxArmorInCell();
        float[][] armorCellsT = this.armorGrid.getGrid();
        this.armorCellsX = armorCellsT.length;
        this.armorCellsY = armorCellsT[0].length;
        this.totalRepairCapacity = this.armorGrid.getArmorRating() * 1.0f * (float)this.armorCellsX * (float)this.armorCellsY / 15.0f;
        this.sinceLastDamage = new float[this.armorCellsX][this.armorCellsY];
        this.armorCells = new float[this.armorCellsX][this.armorCellsY];
        this.expectedRepair = new float[this.armorCellsX][this.armorCellsY];
        this.initialArmorLoaded = false;
    }
}