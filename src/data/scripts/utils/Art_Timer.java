package src.data.scripts.utils;

/*
Just a timer
 */
public class Art_Timer {

    private int value;
    private final float valuemax;

    public Art_Timer(int max) {
        value = 0;
        valuemax = max;
    }
    public Art_Timer(float max) {
        value = 0;
        valuemax = max;
    }

    public void advance() {
        value++;
        if (value > this.valuemax) {
            value = 0;
        }
    }
    
    public void advance(float amount) {
        value += amount;
        if (value > this.valuemax) {
            value = 0;
        }
    }

    public boolean isElapsed() {
        return value == 0;
    }
    
     
}
