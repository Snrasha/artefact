
package src.data.scripts.utils;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.ShipAPI;
import java.awt.Color;
import org.dark.shaders.util.ShaderLib;
import org.dark.shaders.util.TextureData;
import org.dark.shaders.distortion.DistortionShader;
import org.dark.shaders.distortion.RippleDistortion;
import org.dark.shaders.light.LightShader;
import org.dark.shaders.light.StandardLight;
public class Art_GraphicLib_utils {
    
    public static boolean hasGraphicsLib;  
      
    public static void init(){
        hasGraphicsLib = Global.getSettings().getModManager().isModEnabled("shaderLib");  
      
        if (hasGraphicsLib) {  
            ShaderLib.init();  
        } 
    
    }
 
    
    public static void Art_EMPBurstShader(ShipAPI ship, Color outcolor,float powerEMP,float rangeEMP){
        if(hasGraphicsLib){
        
                StandardLight light = new StandardLight(ship.getLocation(), Art_Constants.ZERO, Art_Constants.ZERO, null);
                light.setColor(outcolor);
                light.setSize(rangeEMP);
                light.setIntensity(powerEMP);
                light.fadeOut(0.5f);
                LightShader.addLight(light);
                
                RippleDistortion ripple = new RippleDistortion(ship.getLocation(), Art_Constants.ZERO);
                ripple.setSize(rangeEMP);
                ripple.setIntensity(rangeEMP * 0.1f);
                ripple.setFrameRate(20);
                ripple.fadeInSize(0.5f);
                ripple.fadeOutIntensity(0.5f);
                DistortionShader.addDistortion(ripple);
        }
    }
    
}
