package src.data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ViewportAPI;
import com.fs.starfarer.api.graphics.SpriteAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import java.awt.Color;
import java.util.Iterator;
import java.util.List;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.opengl.Display;
import static org.lwjgl.opengl.GL11.*;
import org.lwjgl.util.vector.Vector2f;
import src.data.scripts.utils.Art_Constants;
import src.data.scripts.utils.Art_GraphicLib_utils;

// Take the projectile bounce from Templar mod.
public class Art_EmpBurstStats extends BaseShipSystemScript {

    public static final float EMPDAMAGE = 200;
    public static final float DAMAGE = 50;
    public static final int TIME = 70;
    public static final float FLUX_COST = 0.25f;
    public static final float RANGEMULT=40f;

    private static final Color INNERCOLOR = new Color(20, 175, 20, 200);

    private static final Color OUTERCOLOR = new Color(100, 255, 100, 255);
    private static final float powerEMP = 1;
    private SpriteAPI sprite = null;
    private float rotation = 0;

    @Override
    public void apply(MutableShipStatsAPI stats, String id, ShipSystemStatsScript.State state, float effectLevel) {

        ShipAPI ship;
        if (stats.getEntity() instanceof ShipAPI) {
            ship = (ShipAPI) stats.getEntity();
        } else {
            return;
        }
        CombatEngineAPI engine = Global.getCombatEngine();

        if (engine == null || engine.isPaused()) {
            return;
        }
        if (ship.getVariant().isDHull()) {
            return;
        }
        float rangeEMP = (float) Math.sqrt(ship.getCollisionRadius()) * RANGEMULT;

        if (sprite == null) {
            sprite = Global.getSettings().getSprite("shockwave", "Art_EMPBurst");
           /*if (rangeEMP >= 128.0f) {
                sprite = Global.getSettings().getSprite("fx", "Art_shieldsInner256");
            } else if (rangeEMP >= 64.0f) {
                sprite = Global.getSettings().getSprite("fx", "Art_shieldsInner128");
            } else {
                sprite = Global.getSettings().getSprite("fx", "Art_shieldsInner64");
            }*/
        }
        if (effectLevel > 0) {
            final Vector2f loc = ship.getLocation();
            final ViewportAPI view = Global.getCombatEngine().getViewport();
            float jitterLevel= effectLevel*1.3f;
            if(jitterLevel>1f)jitterLevel=1f;
            if (view.isNearViewport(loc, rangeEMP)) {
                glPushAttrib(GL_ENABLE_BIT);
                glMatrixMode(GL_PROJECTION);
                glPushMatrix();
                glViewport(0, 0, Display.getWidth(), Display.getHeight());
                glOrtho(0.0, Display.getWidth(), 0.0, Display.getHeight(), -1.0, 1.0);
                glEnable(GL_TEXTURE_2D);
                glEnable(GL_BLEND);
                float radius=(rangeEMP * 2f) / view.getViewMult();
                if(state == State.IN){
                    radius*=jitterLevel;
                }
                sprite.setSize(radius, radius);
                sprite.setAlphaMult(effectLevel);
                sprite.renderAtCenter(view.convertWorldXtoScreenX(loc.x), view.convertWorldYtoScreenY(loc.y));
                sprite.setAngle(rotation);
                glPopMatrix();
                glPopAttrib();
            }
            float amount = Global.getCombatEngine().getElapsedInLastFrame();
            rotation += 25f * amount;
            if (effectLevel >= 1) {
                ship.getFluxTracker().increaseFlux(
                        ship.getMutableStats().getPhaseCloakActivationCostBonus().computeEffective(
                                ship.getMutableStats().getFluxCapacity().getBaseValue() * FLUX_COST * 0.9f),
                        true);
                Global.getSoundPlayer().playSound("tachyon_lance_emp_impact", 1f, powerEMP * 2F, ship.getLocation(),
                        Art_Constants.ZERO);

                Art_GraphicLib_utils.Art_EMPBurstShader(ship, OUTERCOLOR, powerEMP, rangeEMP);
                Iterator<ShipAPI> itership = AIUtils.getNearbyEnemies(ship, rangeEMP).iterator();
                Iterator<CombatEntityAPI> iterasteroid = CombatUtils.getAsteroidsWithinRange(ship.getLocation(), rangeEMP).iterator();
                List<DamagingProjectileAPI> listproj = CombatUtils.getProjectilesWithinRange(ship.getLocation(), rangeEMP);

                for (MissileAPI missile : AIUtils.getNearbyEnemyMissiles(ship, rangeEMP)) {
                    listproj.add(missile);
                }
                Iterator<DamagingProjectileAPI> iterproj = listproj.iterator();

                ShipAPI targetship;
                CombatEntityAPI targetasteroid;
                DamagingProjectileAPI thisProj;

                while (itership.hasNext()) {
                    targetship = itership.next();
                    if (targetship == null || !targetship.isFighter()) {
                        continue;
                    }

                    engine.applyDamage(targetship, targetship.getLocation(), DAMAGE * powerEMP,
                            DamageType.ENERGY, 0, false,
                            false, ship);
                    CombatUtils.applyForce(targetship, VectorUtils.getDirectionalVector(ship.getLocation(),
                            targetship.getLocation()), (0.5f + powerEMP) * (1f - MathUtils.getDistance(ship, targetship) / rangeEMP));
                    engine.addHitParticle(targetship.getLocation(), ship.getVelocity(), powerEMP * 15f, 1f, 0.5f, INNERCOLOR);

                }
                while (iterasteroid.hasNext()) {
                    targetasteroid = iterasteroid.next();
                    if (targetasteroid == null) {
                        continue;
                    }

                    engine.applyDamage(targetasteroid, targetasteroid.getLocation(), DAMAGE * powerEMP,
                            DamageType.ENERGY, 0, false,
                            false, ship);
                    CombatUtils.applyForce(targetasteroid, VectorUtils.getDirectionalVector(ship.getLocation(),
                            targetasteroid.getLocation()), (0.5f + powerEMP) * (1f - MathUtils.getDistance(ship, targetasteroid) / rangeEMP));

                }

                while (iterproj.hasNext()) {
                    thisProj = iterproj.next();
                    if (thisProj == null) {
                        continue;
                    }
                    if (thisProj.getOwner() != ship.getOwner()) {

                        float returnAngle = VectorUtils.getAngle(ship.getLocation(), thisProj.getLocation());
                        thisProj.getVelocity().set(MathUtils.getPointOnCircumference(null, thisProj.getVelocity().length(), returnAngle));
                        thisProj.setFacing(returnAngle);
                        if (thisProj instanceof MissileAPI) {

                            engine.applyDamage(thisProj, thisProj.getLocation(), DAMAGE * powerEMP,
                                    DamageType.ENERGY, EMPDAMAGE * powerEMP, false,
                                    false, ship);
                            engine.addHitParticle(thisProj.getLocation(), ship.getVelocity(), powerEMP * 15f, 1f, 0.5f, INNERCOLOR);
                        } else {
                            thisProj.setOwner(ship.getOwner());
                            thisProj.setSource(ship);

                        }
                    }
                }

            }
        }
    }
}
