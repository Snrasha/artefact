package src.data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;

public class Art_Artefactstats extends BaseShipSystemScript{

    public static final float ROF_BONUS = 1f;
    
    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
            if (state == ShipSystemStatsScript.State.OUT) {
                    stats.getMaxSpeed().unmodify(id); // to slow down ship to its regular top speed while powering drive down
                    stats.getMaxTurnRate().unmodify(id);
            } else {
                    float mult = 0.5f + ROF_BONUS * effectLevel;
                    stats.getEnergyRoFMult().modifyMult(id, mult);
                    stats.getMaxSpeed().modifyFlat(id, 50f);
                    stats.getMaxSpeed().modifyMult(id, 1.5f * effectLevel);
                    stats.getAcceleration().modifyMult(id, 1.5f * effectLevel);
                    stats.getDeceleration().modifyMult(id, 1.5f * effectLevel);
                    
            }
    }
    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
            stats.getMaxSpeed().unmodify(id);
            stats.getAcceleration().unmodify(id);
            stats.getDeceleration().unmodify(id);
            stats.getEnergyRoFMult().unmodify(id);
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
            float mult = 1f + ROF_BONUS * effectLevel;
            float bonusPercent = (int) ((mult - 1.5f) * 100f);
            if (index == 0) {
			return new StatusData("energy rate of fire +" + (int) bonusPercent + "%", false);
		}
            if (index == 1) {
                    return new StatusData("improved reactor", false);
            } else if (index == 2) {
                    return new StatusData("+50 top speed", false);
            }
            return null;
    }

}
