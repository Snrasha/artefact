package src.data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipEngineControllerAPI.ShipEngineAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import java.awt.Color;
import org.lwjgl.util.vector.Vector2f;
import src.data.scripts.utils.Art_Constants;

public class Art_system_pe extends BaseShipSystemScript {

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        if (state == ShipSystemStatsScript.State.OUT) {
            stats.getMaxSpeed().unmodify(id); // to slow down ship to its regular top speed while powering drive down
        } else {
            stats.getMaxSpeed().modifyFlat(id, 800f);
            stats.getAcceleration().modifyFlat(id, 800f);
        }
        if (State.ACTIVE == state) {
            if (effectLevel > 0f || effectLevel <= 0.1f) {
                ShipAPI ship;
                if (stats.getEntity() instanceof ShipAPI) {
                    ship = (ShipAPI) stats.getEntity();
                } else {
                    return;
                }
                CombatEngineAPI engine = Global.getCombatEngine();

                if (engine == null || engine.isPaused()) {
                    return;
                }
                Color color = ship.getEngineController().getShipEngines().get(0).getEngineColor();
                Color color2 = ship.getEngineController().getShipEngines().get(0).getContrailColor();

                Vector2f loc;
                for (ShipEngineAPI engin : ship.getEngineController().getShipEngines()) {
                    loc = engin.getLocation();
                    engine.spawnExplosion(loc, Art_Constants.ZERO, color2, 10, 0.15f);
                    engine.addHitParticle(loc, Art_Constants.ZERO, 5, 0.3f, 1f, color);
                }

            }
        }

    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getMaxSpeed().unmodify(id);
        stats.getMaxTurnRate().unmodify(id);
        stats.getTurnAcceleration().unmodify(id);
        stats.getAcceleration().unmodify(id);
        stats.getDeceleration().unmodify(id);
    }


}
