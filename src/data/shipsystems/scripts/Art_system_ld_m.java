package src.data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import java.awt.Color;

public class Art_system_ld_m extends BaseShipSystemScript {

    public static final Object KEY_JITTER = new Object();

    public static final float DAMAGE_TAKEN_PERCENT = 20;
    public static final float TOPSPEEDREDUCED_PERCENT = 30;
    public static final Color JITTER_UNDER_COLOR = new Color(0, 255, 255, 255);
    public static final Color JITTER_COLOR = new Color(0, 0, 255, 125);

    @Override
    public void apply(MutableShipStatsAPI stats, String id, ShipSystemStatsScript.State state, float effectLevel) {
        ShipAPI ship = null;
        if (stats.getEntity() instanceof ShipAPI) {
            ship = (ShipAPI) stats.getEntity();
        } else {
            return;
        }

        if (effectLevel > 0) {
            stats.getHullDamageTakenMult().modifyMult(id, 1f + 0.01f * DAMAGE_TAKEN_PERCENT * effectLevel);
            stats.getArmorDamageTakenMult().modifyMult(id, 1f + 0.01f * DAMAGE_TAKEN_PERCENT * effectLevel);
            stats.getEmpDamageTakenMult().modifyMult(id, 1f + 0.01f * DAMAGE_TAKEN_PERCENT * effectLevel);
            stats.getMaxSpeed().modifyMult(id, 1f - 0.01f * TOPSPEEDREDUCED_PERCENT * effectLevel);

            float jitterLevel = effectLevel;
            float jitterRangeBonus = jitterLevel * 2f;
            float jitterUnderRangeBonus = jitterLevel * 5f;
            

            if (jitterLevel > 0) {

                ship.setJitterUnder(KEY_JITTER, JITTER_UNDER_COLOR, jitterLevel, 1, jitterRangeBonus, jitterUnderRangeBonus);
               // ship.setJitter(KEY_JITTER, JITTER_COLOR, jitterLevel, 1, 0f, jitterRangeBonus);

            }

        }

    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {

        stats.getHullDamageTakenMult().unmodify(id);
        stats.getArmorDamageTakenMult().unmodify(id);
        stats.getEmpDamageTakenMult().unmodify(id);
        stats.getMaxSpeed().unmodify(id);
    }

    @Override
    public ShipSystemStatsScript.StatusData getStatusData(int index, ShipSystemStatsScript.State state, float effectLevel) {

        if (index == 0) {
            return new StatusData((int) DAMAGE_TAKEN_PERCENT + "% less damage taken", false);
        }
        if (index == 1) {
            return new StatusData((int) TOPSPEEDREDUCED_PERCENT + "% less top speed", false);
        }
        return null;
    }

}
