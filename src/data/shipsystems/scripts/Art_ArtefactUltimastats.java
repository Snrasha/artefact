package src.data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BaseEveryFrameCombatPlugin;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAPI;

import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.input.InputEventAPI;
import java.awt.Color;
import java.util.List;
import org.lazywizard.lazylib.combat.AIUtils;

// Multiple AcausalDisruptorStats
public class Art_ArtefactUltimastats extends BaseShipSystemScript {

    public static final float DISRUPTION_DUR = 1f;
    public static final float MIN_DISRUPTION_RANGE = 1400f;

    public static final Color OVERLOAD_COLOR = new Color(255, 155, 255, 255);

    public static final Color JITTER_COLOR = new Color(255, 155, 255, 75);
    public static final Color JITTER_UNDER_COLOR = new Color(255, 155, 255, 155);

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        ShipAPI ship = null;
        if (stats.getEntity() instanceof ShipAPI) {
            ship = (ShipAPI) stats.getEntity();
        } else {
            return;
        }

        if (effectLevel > 0f) {

            float jitterLevel = effectLevel / 4;
            if (state == State.OUT) {
                jitterLevel *= jitterLevel;
            }
            float maxRangeBonus = 50f;
            float jitterRangeBonus = jitterLevel * maxRangeBonus;

            ship.setJitterUnder(this, JITTER_UNDER_COLOR, jitterLevel, 21, 0f, 3f + jitterRangeBonus);
            ship.setJitter(this, JITTER_COLOR, jitterLevel, 4, 0f, 0 + jitterRangeBonus);

            if (ship.getSystem().isChargeup()) {
                String targetKey = ship.getId() + "_acausal_target";
                Object foundTarget = Global.getCombatEngine().getCustomData().get(targetKey);

                if (foundTarget == null) {
                    List<ShipAPI> ships = AIUtils.getNearbyEnemies(ship, MIN_DISRUPTION_RANGE);
                    if (!ships.isEmpty()) {
                        Global.getCombatEngine().getCustomData().put(targetKey, ships);
                    }
                }
            } else if (ship.getSystem().isChargedown()) {
                String targetKey = ship.getId() + "_acausal_target";
                Object foundTarget = Global.getCombatEngine().getCustomData().get(targetKey);
                if (foundTarget != null) {
                    if (foundTarget instanceof List) {
                        List<CombatEntityAPI> cbmentity = (List<CombatEntityAPI>) foundTarget;
                        if (!cbmentity.isEmpty() && cbmentity.get(0) instanceof ShipAPI) {
                            List<ShipAPI> ships = (List<ShipAPI>) foundTarget;
                            for (ShipAPI target : ships) {
                                if (target.getFluxTracker().isOverloaded()) {
                                    continue;
                                }
                                applyEffectToTarget(ship, target);
                            }
                            Global.getCombatEngine().getCustomData().remove(targetKey);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
    }

    protected void applyEffectToTarget(final ShipAPI ship, final ShipAPI target) {
        if (target.getFluxTracker().isOverloadedOrVenting()) {
            return;
        }
        if (target == ship) {
            return;
        }

        target.setOverloadColor(OVERLOAD_COLOR);
        target.getFluxTracker().forceOverload(DISRUPTION_DUR);
        target.getVelocity().scale(0.1f);
        //target.getFluxTracker().beginOverloadWithTotalBaseDuration(DISRUPTION_DUR);
        //target.getEngineController().forceFlameout(true);

        Global.getCombatEngine().spawnEmpArc(ship, ship.getLocation(), target, target,
                DamageType.ENERGY,
                0,
                0,
                MIN_DISRUPTION_RANGE,
                "tachyon_lance_emp_impact",
                MIN_DISRUPTION_RANGE / 100,
                JITTER_COLOR,
                JITTER_UNDER_COLOR
        );

        if (target.getFluxTracker().showFloaty()
                || ship == Global.getCombatEngine().getPlayerShip()
                || target == Global.getCombatEngine().getPlayerShip()) {
            target.getFluxTracker().playOverloadSound();
            target.getFluxTracker().showOverloadFloatyIfNeeded("System Disruption!", OVERLOAD_COLOR, 4f, true);
        }

        Global.getCombatEngine().addPlugin(new BaseEveryFrameCombatPlugin() {
            @Override
            public void advance(float amount, List<InputEventAPI> events) {
                if (!target.getFluxTracker().isOverloaded()) {
                    target.resetOverloadColor();
                    Global.getCombatEngine().removePlugin(this);
                }
            }
        });
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        return null;
    }

    @Override
    public boolean isUsable(ShipSystemAPI system, ShipAPI ship) {
        List<ShipAPI> target = AIUtils.getNearbyEnemies(ship, MIN_DISRUPTION_RANGE);
        return !target.isEmpty();
    }

    @Override
    public String getInfoText(ShipSystemAPI system, ShipAPI ship) {
        if (system.isOutOfAmmo()) {
            return null;
        }
        if (system.getState() != ShipSystemAPI.SystemState.IDLE) {
            return null;
        }
        return super.getInfoText(system, ship);
    }
}
