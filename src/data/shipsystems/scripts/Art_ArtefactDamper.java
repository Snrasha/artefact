package src.data.shipsystems.scripts;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;

public class Art_ArtefactDamper extends BaseShipSystemScript {

    public static final Object KEY_JITTER = new Object();

    public static final float DAMAGE_TAKEN_PERCENT = 25;
    public static final float DAMAGE_DECREASE_PERCENT = 25;

    public static final Color JITTER_UNDER_COLOR = new Color(90, 165, 255, 155);
    public static final Color JITTER_COLOR = new Color(90, 255, 165, 155);

    @Override
    public void apply(MutableShipStatsAPI stats, String id, ShipSystemStatsScript.State state, float effectLevel) {
        ShipAPI ship = null;
        if (stats.getEntity() instanceof ShipAPI) {
            ship = (ShipAPI) stats.getEntity();
        } else {
            return;
        }

        if (effectLevel > 0) {
            stats.getHullDamageTakenMult().modifyMult(id, 1f + 0.01f * DAMAGE_TAKEN_PERCENT * effectLevel);
            stats.getArmorDamageTakenMult().modifyMult(id, 1f + 0.01f * DAMAGE_TAKEN_PERCENT * effectLevel);
            stats.getEmpDamageTakenMult().modifyMult(id, 1f + 0.01f * DAMAGE_TAKEN_PERCENT * effectLevel);
            stats.getBallisticWeaponDamageMult().modifyMult(id, 1f - 0.01f * DAMAGE_DECREASE_PERCENT * effectLevel);
            stats.getEnergyWeaponDamageMult().modifyMult(id, 1f - 0.01f * DAMAGE_DECREASE_PERCENT * effectLevel);
            stats.getMissileWeaponDamageMult().modifyMult(id, 1f - 0.01f * DAMAGE_DECREASE_PERCENT * effectLevel);

            float jitterLevel = effectLevel;
            float maxRangeBonus = 5f;
            float jitterRangeBonus = jitterLevel * maxRangeBonus;
            for (ShipAPI fighter : getFighters(ship)) {
                if (fighter.isHulk()) {
                    continue;
                }
                MutableShipStatsAPI fStats = fighter.getMutableStats();
                fStats.getHullDamageTakenMult().modifyMult(id, 1f + 0.01f * DAMAGE_TAKEN_PERCENT * effectLevel);
                fStats.getArmorDamageTakenMult().modifyMult(id, 1f + 0.01f * DAMAGE_TAKEN_PERCENT * effectLevel);
                fStats.getEmpDamageTakenMult().modifyMult(id, 1f + 0.01f * DAMAGE_TAKEN_PERCENT * effectLevel);
                fStats.getBallisticWeaponDamageMult().modifyMult(id, 1f - 0.01f * DAMAGE_DECREASE_PERCENT * effectLevel);
                fStats.getEnergyWeaponDamageMult().modifyMult(id, 1f - 0.01f * DAMAGE_DECREASE_PERCENT * effectLevel);
                fStats.getMissileWeaponDamageMult().modifyMult(id, 1f - 0.01f * DAMAGE_DECREASE_PERCENT * effectLevel);

                if (jitterLevel > 0) {

                    fighter.setJitterUnder(KEY_JITTER, JITTER_COLOR, jitterLevel, 5, 0f, jitterRangeBonus);
                    fighter.setJitter(KEY_JITTER, JITTER_UNDER_COLOR, jitterLevel, 2, 0f, 0 + jitterRangeBonus * 1f);
                    Global.getSoundPlayer().playLoop("system_targeting_feed_loop", ship, 1f, 1f, fighter.getLocation(), fighter.getVelocity());
                }
            }
        }
    }

    private List<ShipAPI> getFighters(ShipAPI carrier) {
        List<ShipAPI> result = new ArrayList<>();

        for (ShipAPI ship : Global.getCombatEngine().getShips()) {
            if (!ship.isFighter()) {
                continue;
            }
            if (ship.getWing() == null) {
                continue;
            }
            if (ship.getWing().getSourceShip() == carrier) {
                result.add(ship);
            }
        }

        return result;
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        ShipAPI ship = null;

        if (stats.getEntity() instanceof ShipAPI) {
            ship = (ShipAPI) stats.getEntity();
        } else {
            return;
        }
        stats.getHullDamageTakenMult().unmodify(id);
        stats.getArmorDamageTakenMult().unmodify(id);
        stats.getEmpDamageTakenMult().unmodify(id);
        for (ShipAPI fighter : getFighters(ship)) {
            if (fighter.isHulk()) {
                continue;
            }
            MutableShipStatsAPI fStats = fighter.getMutableStats();
            fStats.getBallisticWeaponDamageMult().unmodify(id);
            fStats.getEnergyWeaponDamageMult().unmodify(id);
            fStats.getMissileWeaponDamageMult().unmodify(id);
        }
    }

    @Override
    public ShipSystemStatsScript.StatusData getStatusData(int index, ShipSystemStatsScript.State state, float effectLevel) {

        
        if (index == 0) {
            return new StatusData((int) DAMAGE_TAKEN_PERCENT + "% less damage taken", false);
        }
        if (index == 1) {
            return new StatusData((int) DAMAGE_DECREASE_PERCENT + "% less weapon damage", false);
        }
        return null;
    }

}
