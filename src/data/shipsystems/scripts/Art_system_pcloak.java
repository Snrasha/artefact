package src.data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import java.awt.Color;

public class Art_system_pcloak extends BaseShipSystemScript {

    public static final float SHIP_ALPHA_MULT = 0.25f;
    public static final float VULNERABLE_FRACTION = 0f;
    protected Object STATUSKEY2 = new Object();
    protected Object KEY_JITTER = new Object();

    public static final Color JITTER_UNDER_COLOR = new Color(255, 0, 255, 255);
    public static final Color JITTER_COLOR = new Color(255, 0, 255, 125);

    protected void maintainStatus(ShipAPI playerShip, State state, float effectLevel) {
        float level = effectLevel;
        float f = VULNERABLE_FRACTION;

        ShipSystemAPI cloak = playerShip.getPhaseCloak();
        if (cloak == null) {
            cloak = playerShip.getSystem();
        }
        if (cloak == null) {
            return;
        }

        if (level > f) {
            Global.getCombatEngine().maintainStatusForPlayerShip(STATUSKEY2,
                    cloak.getSpecAPI().getIconSpriteName(), cloak.getDisplayName(), "Pulse cloak ON " + (int) (level * 100) + "%", false);
        }

    }

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        ShipAPI ship;
        boolean player;
        if (stats.getEntity() instanceof ShipAPI) {
            ship = (ShipAPI) stats.getEntity();
            player = ship == Global.getCombatEngine().getPlayerShip();
            id = id + "_" + ship.getId();
        } else {
            return;
        }

        if (Global.getCombatEngine().isPaused()) {
            return;
        }

        if (player) {
            maintainStatus(ship, state, effectLevel);
        }

        if (state == State.COOLDOWN || state == State.IDLE) {
            unapply(stats, id);
            return;
        }
        /*
        Float fl = affected.get(ship);
        if (fl == null) {
            affected.put(ship, 0f);
        } else if (fl < TIMER) {
            fl += 0.06f;

            affected.put(ship, fl);
        } else {
            ship.useSystem();
            affected.remove(ship);
        }
        ship.useSystem();
         */

        if ((ship.getFluxTracker().getCurrFlux() / ship.getFluxTracker().getMaxFlux()) > 0.95f) {
            ship.getPhaseCloak().deactivate();
        }
        float level = effectLevel;
        float levelForAlpha = level;
        if (state == State.IN || state == State.ACTIVE) {
            ship.setPhased(true);
            levelForAlpha = level;

        } else if (state == State.OUT) {
            ship.setPhased(true);
            levelForAlpha = level;
        }

        ship.setExtraAlphaMult(1f - (1f - SHIP_ALPHA_MULT) * levelForAlpha);
        ship.setApplyExtraAlphaToEngines(true);
        if (effectLevel > 0) {
            float jitterLevel = effectLevel;
            float jitterRangeBonus = jitterLevel / 10f;
            if (jitterLevel > 0) {
                ship.setJitter(KEY_JITTER, JITTER_COLOR, jitterLevel, 1, 0f, jitterRangeBonus);
            }
        }
    }

    @Override
    public boolean isUsable(ShipSystemAPI system, ShipAPI ship) {
        return (ship.getFluxTracker().getCurrFlux() / ship.getFluxTracker().getMaxFlux()) < 0.80f;

    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        ShipAPI ship;
        if (stats.getEntity() instanceof ShipAPI) {
            ship = (ShipAPI) stats.getEntity();
        } else {
            return;
        }

        Global.getCombatEngine().getTimeMult().unmodify(id);
        stats.getTimeMult().unmodify(id);

        ship.setPhased(false);
        ship.setExtraAlphaMult(1f);
        // affected.remove(ship);
    }

}
