package src.data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipEngineControllerAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import java.util.List;
import org.lwjgl.util.vector.Vector2f;

public class Art_ai_lo implements ShipSystemAIScript {

    private CombatEngineAPI engine;
    private ShipAPI ship;
    private ShipSystemAPI system;

    private final IntervalUtil tracker = new IntervalUtil(0.1f, 0.2f);

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (engine == null) {
            return;
        }

        if (engine.isPaused()) {
            return;
        }

        tracker.advance(amount);

        if (tracker.intervalElapsed()) {
            if (ship.getFluxTracker().isOverloadedOrVenting()) {
                return;
            }
            if (this.ship.isRetreating()) {
                if (!system.isOn()) {
                    ship.useSystem();
                }
            }
            
            ShipEngineControllerAPI shipengine = this.ship.getEngineController();
            if (shipengine.isDisabled() || shipengine.isFlamedOut() || shipengine.isFlamingOut()) {
                if (!system.isOn()) {
                    ship.useSystem();
                }
            }
            int flag = 0;
            List<WeaponAPI> weapons = this.ship.getAllWeapons();
            for (WeaponAPI weapon : weapons) {
                if (weapon.isDisabled() || !weapon.isPermanentlyDisabled()) {
                    flag++;
                }
            }
            if (flag > (weapons.size() / 2)) {
                if (!system.isOn()) {
                    ship.useSystem();
                }
            }
        }
    }

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.system = system;
        this.engine = engine;
    }
}
