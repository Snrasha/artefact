package src.data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.ShipwideAIFlags.AIFlags;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

public class Art_AI_ad implements ShipSystemAIScript {

    private CombatEngineAPI engine;
    private ShipwideAIFlags flags;
    private ShipAPI ship;
    private ShipSystemAPI system;

    private final IntervalUtil tracker = new IntervalUtil(0.1f, 0.2f);

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (engine == null) {
            return;
        }

        if (engine.isPaused()) {
            return;
        }

        tracker.advance(amount);

        if (tracker.intervalElapsed()) {
            if (ship.getFluxTracker().isOverloadedOrVenting()) {
                return;
            }
            
            boolean decision = false;
         //   float decisionLevel = 0f;
            
             if(missileDangerDir!=null)
            if((MathUtils.getDistance(missileDangerDir, ship.getLocation())-ship.getCollisionRadius())<100){
                  //decisionLevel +=5.5f;
                  decision=true;
            }
             if(collisionDangerDir!=null)
            if((MathUtils.getDistance(collisionDangerDir, ship.getLocation())-ship.getCollisionRadius())<50){
               //   decisionLevel +=5.5f;
                  decision=true;
            }
     
            if (flags.hasFlag(AIFlags.IN_CRITICAL_DPS_DANGER)) {
              //  decisionLevel += 2.5f;
                   decision=true;
            }
            if (flags.hasFlag(AIFlags.HAS_INCOMING_DAMAGE)) {
               // decisionLevel += 2.5f;
                   decision=true;
            }
            /*
            if (flags.hasFlag(AIFlags.MAINTAINING_STRIKE_RANGE)) {
              //  decisionLevel -= 1f;
                   decision=false;
            }*/
            /*
            if (flags.hasFlag(AIFlags.SAFE_FROM_DANGER_TIME)) {
                decisionLevel -= 1f;
            }

            if (flags.hasFlag(AIFlags.CARRIER_FIGHTER_TARGET)) {
                decisionLevel -= 2f;
            }*/
            if (ship.isRetreating()) {
              //  decisionLevel += 4f;
                   decision=true;
            }
            
            if (flags.hasFlag(AIFlags.NEEDS_HELP)) {
               // decisionLevel += 1.5f;
                   decision=true;
            }
            /*
            if (flags.hasFlag(AIFlags.SAFE_FROM_DANGER_TIME)) {
                decisionLevel -= 1.5f;
            }*/
            if(ship.getHitpoints()<(ship.getMaxHitpoints()/3) && ship.getFluxTracker().getHardFlux()>(ship.getFluxTracker().getMaxFlux()/2f)){
                decision=true;
               /* decisionLevel +=2f;*/
            }
            /*if(ship.getFluxTracker().getHardFlux()>(ship.getFluxTracker().getMaxFlux()/2f)){
                //decisionLevel +=2f;
            }
            if (flags.hasFlag(AIFlags.PURSUING)) {
              //  decisionLevel -= 1.5f;
            }*/
            if (system.isOn()) {
                if (!decision) {
                    ship.useSystem();
                }
            } else {
                if (decision) {
                    ship.useSystem();
                }
            }
/*
            if (system.isOn()) {
                
                if (decisionLevel <= 0.4f) {
                    ship.useSystem();
                }
            } else {
                if (decisionLevel >= 0.6f) {
                    ship.useSystem();
                }
            }*/
        }
    }

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.flags = flags;
        this.system = system;
        this.engine = engine;
    }
}
