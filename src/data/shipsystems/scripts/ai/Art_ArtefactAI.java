package src.data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.ShipwideAIFlags.AIFlags;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

public class Art_ArtefactAI implements ShipSystemAIScript {

    private CombatEngineAPI engine;
    private ShipwideAIFlags flags;
    private ShipAPI ship;
    private ShipSystemAPI system;

    private final IntervalUtil tracker = new IntervalUtil(0.1f, 0.2f);

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (engine == null) {
            return;
        }

        if (engine.isPaused()) {
            return;
        }

        tracker.advance(amount);

        if (tracker.intervalElapsed()) {
            if (ship.getFluxTracker().isOverloadedOrVenting()) {
                return;
            }

            float decisionLevel = 0f;
            if (flags.hasFlag(AIFlags.RUN_QUICKLY)) {
                decisionLevel += 2.5f;
            } else if (flags.hasFlag(AIFlags.PURSUING)) {
                decisionLevel += 1.75f;
            } else if (ship.getShipTarget() == null || MathUtils.getDistance(ship.getShipTarget(), ship) > 2000f) {
                decisionLevel += 0.75f;
            }
            if (flags.hasFlag(AIFlags.TURN_QUICKLY)) {
                decisionLevel -= 0.5f;
            }
            if (flags.hasFlag(AIFlags.BACK_OFF)) {
                decisionLevel += 0.5f;
            }
            if (flags.hasFlag(AIFlags.DO_NOT_USE_FLUX)) {
                decisionLevel -= 5f;
            }
            if (flags.hasFlag(AIFlags.HAS_INCOMING_DAMAGE)) {
                decisionLevel -= 0.5f;
            }

            decisionLevel -= ship.getFluxTracker().getFluxLevel() * ship.getFluxTracker().getFluxLevel() * 2f;
            decisionLevel -= ship.getFluxTracker().getHardFlux() / ship.getFluxTracker().getMaxFlux();

            if (system.isOn()) {
                if (decisionLevel <= 0.4f) {
                    ship.useSystem();
                }
            } else {
                if (decisionLevel >= 0.6f) {
                    ship.useSystem();
                }
            }
        }
    }

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.flags = flags;
        this.system = system;
        this.engine = engine;
    }
}
