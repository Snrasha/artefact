package src.data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.DamagingProjectileAPI;
import com.fs.starfarer.api.combat.FluxTrackerAPI;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import java.util.List;
import org.lazywizard.lazylib.CollectionUtils;
import org.lazywizard.lazylib.CollectionUtils.CollectionFilter;
import org.lazywizard.lazylib.CollisionUtils;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lazywizard.lazylib.combat.CombatUtils;
import org.lwjgl.util.vector.Vector2f;
import src.data.scripts.utils.Art_Timer;
import src.data.shipsystems.scripts.Art_EmpBurstStats;


// PART OF THE AI SHIPSYSTEM FROM EXIGENCY have be taken. A tiny bit authorized.
public class Art_ai_EMPBurst implements ShipSystemAIScript {
  private ShipAPI ship;
    private final Art_Timer tracker = new Art_Timer(0.35f);
    private static final float SECONDS_TO_LOOK_AHEAD = 3f;
    private CombatEngineAPI engine = null;
    
    // From Exigency, i am pretty too bad for made AI....
    private final CollectionFilter<DamagingProjectileAPI> filterMisses = new CollectionFilter<DamagingProjectileAPI>() {
        @Override
        public boolean accept(DamagingProjectileAPI proj) {
            // Exclude missiles and our own side's shots
            if (proj instanceof MissileAPI || proj.getOwner() == ship.getOwner()) {
                return false;
            }
            // Don't do anything if the projectile does zero damage (probably a fake)
            if (proj.getBaseDamageAmount() <= 0) {
                return false;
            }
            // Only include shots that are on a collision path with us
            // Also ensure they aren't travelling AWAY from us ;)
            return (CollisionUtils.getCollides(proj.getLocation(), Vector2f.add(proj.getLocation(), (Vector2f) new Vector2f(proj.getVelocity()).scale(
                    SECONDS_TO_LOOK_AHEAD), null), ship.getLocation(), ship.getCollisionRadius())
                    && Math.abs(MathUtils.getShortestRotation(proj.getFacing(), VectorUtils.getAngle(proj.getLocation(), ship.getLocation()))) <= 90f);
        }
    };
  
    // From AIUtils, but needed to be for phase cloak.
    public static boolean canUsePhaseThisFrame(ShipAPI ship) {
        FluxTrackerAPI flux = ship.getFluxTracker();
        ShipSystemAPI system = ship.getPhaseCloak();

        // No system, overloading/venting, out of ammo
        return !(system == null || ship.getVariant().isDHull() || flux.isOverloadedOrVenting() || system.isOutOfAmmo()
                // In use but can't be toggled off right away
                || (system.isOn() && system.getCooldownRemaining() > 0f)
                // In chargedown, in cooldown
                || (system.isActive() && !system.isOn()) || system.getCooldownRemaining() > 0f
                // Not enough flux + 1.1f for be sure.
                || (Art_EmpBurstStats.FLUX_COST*flux.getMaxFlux()*1.1f) > (flux.getMaxFlux() - flux.getCurrFlux()));
    }
    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if (engine == null) {
            return;
        }

        
        tracker.advance(amount);
        Vector2f shipLoc = ship.getLocation();
        if (tracker.isElapsed()) {
            
            // Can we even use the system right now?
            if (!canUsePhaseThisFrame(ship)) {
                return;
            }
            boolean shouldUseSystem = false;
            float range = (float) Math.sqrt(ship.getCollisionRadius()) * (Art_EmpBurstStats.RANGEMULT+5);
            // Because Missiles are sometimes too far.
            range *= 0.65f;
            List<DamagingProjectileAPI> nearbyThreats = CombatUtils.getProjectilesWithinRange(shipLoc, range); // Use this to try and defend against weapons fire
            nearbyThreats = CollectionUtils.filter(nearbyThreats, filterMisses);
            for (MissileAPI missile : AIUtils.getNearbyEnemyMissiles(ship, range)) {
                if (missile.isFlare()) {
                    continue;
                }
                nearbyThreats.add(missile);
            }
            float damage=0;
            for(DamagingProjectileAPI threat:nearbyThreats){
                damage+=threat.getBaseDamageAmount();
            }
            if (!nearbyThreats.isEmpty() && damage>(ship.getHitpoints()/10)) {
                shouldUseSystem = true;
            }

            // If system is inactive and should be active, enable it
            if (shouldUseSystem) {
                ship.giveCommand(ShipCommand.TOGGLE_SHIELD_OR_PHASE_CLOAK, null, 0);
            }
        }
    }

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.engine = engine;
        this.ship = ship;
    }

}
