package src.data.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import java.util.HashMap;
import java.util.Map;

public class Art_Armor extends BaseHullMod {

    private static final int EMPDAMAGE = 35;//50
    private static final int WEAPONHEALTH = 35;//50
    private static final int CREWLOSS = 10;
    private static final int CRMALFUNCTION = 25;
    private static final int HEDAMAGE = 10;//50
    private static final int DAMAGE = 10; //25;
    private static Map mag = new HashMap();

    static {
        mag.put(HullSize.FRIGATE, 0f);
        mag.put(HullSize.DESTROYER, 5f);
        mag.put(HullSize.CRUISER, 10f);
        mag.put(HullSize.CAPITAL_SHIP, 15f);
    }

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getEmpDamageTakenMult().modifyMult(id, 1 - EMPDAMAGE / 100);
        stats.getWeaponHealthBonus().modifyPercent(id, WEAPONHEALTH);

        if (hullSize != null && mag.get(hullSize) != null) {
            stats.getBallisticWeaponRangeBonus().modifyPercent(id, 0 + (Float) mag.get(hullSize));
        }

        stats.getCrewLossMult().modifyMult(id, 1 - CREWLOSS / 100);
        stats.getCriticalMalfunctionChance().modifyMult(id, 1 - CRMALFUNCTION / 100);
        stats.getHighExplosiveDamageTakenMult().modifyMult(id, 1 - HEDAMAGE / 100);
        stats.getArmorDamageTakenMult().modifyMult(id, 1 - DAMAGE / 100);

    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize) {

        if (index == 0) {
            return "" + EMPDAMAGE;
        }
        if (index == 1) {
            return "" + HEDAMAGE;
        }
        if (index == 2) {
            return "" + DAMAGE;
        }
        if (index == 3) {
            return "" + WEAPONHEALTH;
        }
        if (index == 4) {
            return "" + ((Float) mag.get(HullSize.FRIGATE)).intValue();
        }
        if (index == 5) {
            return "" + ((Float) mag.get(HullSize.DESTROYER)).intValue();
        }
        if (index == 6) {
            return "" + ((Float) mag.get(HullSize.CRUISER)).intValue();
        }
        if (index == 7) {
            return "" + ((Float) mag.get(HullSize.CAPITAL_SHIP)).intValue();
        }
        return null;
    }


    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return true;
    }
}
