package src.data.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.ArmorGridAPI;
import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.combat.WeaponAPI;
import java.awt.Color;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import org.lazywizard.lazylib.FastTrig;

// Use the Lattice shield hullmod of Templar, authorized per DR.
public class Art_LotusShield extends BaseHullMod {

    public static final float PERCENT_LIFEMAX = 0.2f;
    public static final float PERCENT_LIFEREGEN = 0.005f;
    public static final float TIME_REGEN_COOLDOWN = 0.01f;
    public static final float REDUC_DAMAGE_BASE = 0.8f;
    public static final float REDUC_DAMAGE_MIN = 0.1f;

    public static final float EMPTY_THRESHOLD = 0.1f;
    private static final float CRITICAL_THRESHOLD = 0.2f;
    private static final float WARNING_THRESHOLD = 0.3f;

    public static final Color CRITICAL_SHIELD_COLOR = new Color(255, 100, 100);
    public static final Color VISUAL_SHIELD_COLOR = new Color(100, 255, 100);

    public static final String ID = "Art_lotusshield";
    private static final String DATA_KEY = "Art_lotusshield";

    private static final Set<String> BLOCKED_HULLMODS = new HashSet<>(1);
    private static final Map<HullSize, Float> PITCHBEND = new EnumMap<>(HullSize.class);
    private static final Map<HullSize, Float> VOLUME = new EnumMap<>(HullSize.class);

    public static final Map<ShipAPI, ShieldState> shipShield = new WeakHashMap<>(100);
    private static final float AUTOREGENLIMIT = 0.005f;
    private static final Map<HullSize, Float> SHIELDREGENTIMER = new EnumMap<>(HullSize.class);

    static {
        BLOCKED_HULLMODS.add("frontshield");
    }

    static {
        PITCHBEND.put(HullSize.FIGHTER, 1.125f);
        PITCHBEND.put(HullSize.FRIGATE, 1f);
        PITCHBEND.put(HullSize.DESTROYER, 0.9f);
        PITCHBEND.put(HullSize.CRUISER, 0.825f);
        PITCHBEND.put(HullSize.CAPITAL_SHIP, 0.775f);
        PITCHBEND.put(HullSize.DEFAULT, 0.775f);
    }

    static {
        VOLUME.put(HullSize.FIGHTER, 0.4f);
        VOLUME.put(HullSize.FRIGATE, 0.75f);
        VOLUME.put(HullSize.DESTROYER, 1f);
        VOLUME.put(HullSize.CRUISER, 1.22f);
        VOLUME.put(HullSize.CAPITAL_SHIP, 1.4f);
        VOLUME.put(HullSize.DEFAULT, 1.4f);
    }

    static {
        SHIELDREGENTIMER.put(HullSize.FIGHTER, 5f);
        SHIELDREGENTIMER.put(HullSize.FRIGATE, 5f);
        SHIELDREGENTIMER.put(HullSize.DESTROYER, 6f);
        SHIELDREGENTIMER.put(HullSize.CRUISER, 8f);
        SHIELDREGENTIMER.put(HullSize.CAPITAL_SHIP, 10f);
        SHIELDREGENTIMER.put(HullSize.DEFAULT, 10f);
    }

    private static float[][] armorArray(ShipAPI ship) {
        if (ship == null || !Global.getCombatEngine().isEntityInPlay(ship)) {
            return null;
        }
        ArmorGridAPI armorGrid = ship.getArmorGrid();
        float[][] armorArray = new float[ship.getArmorGrid().getGrid().length][ship.getArmorGrid().getGrid()[0].length];
        for (int x = 0; x < armorGrid.getGrid().length; x++) {
            for (int y = 0; y < armorGrid.getGrid()[x].length; y++) {
                armorArray[x][y] = armorGrid.getArmorValue(x, y);
            }
        }
        return armorArray;
    }

    private float effectiveRadius(ShipAPI ship) {
        if (ship.getSpriteAPI() == null || ship.isPiece()) {
            return ship.getCollisionRadius();
        } else {
            float fudgeFactor = 1.5f;
            return ((ship.getSpriteAPI().getWidth() / 2f) + (ship.getSpriteAPI().getHeight() / 2f)) * 0.5f * fudgeFactor;
        }
    }

    @Override
    public void advanceInCombat(ShipAPI ship, float amount) {
        CombatEngineAPI engine = Global.getCombatEngine();
        if (!engine.getCustomData().containsKey(DATA_KEY)) {
            engine.getCustomData().put(DATA_KEY, new LocalData());
        }
        if (ship == null) {
            return;
        }

        if (!Global.getCombatEngine().isEntityInPlay(ship)) {
            return;
        }

        ShieldState shieldState = null;
        if (shipShield.containsKey(ship)) {
            shieldState = shipShield.get(ship);
        } else {
            shieldState = shipShield.put(ship, new ShieldState(ship, armorArray(ship), this.effectiveRadius(ship)));
        }
        if (shieldState == null) {
            return;
        }
        if (!ship.isAlive()) {
            shieldState.hitpoint = 0;
            if (shieldState.shieldInter != null) {
                shieldState.shieldInter.getAnimation().setFrame(0);
            }
        } else {
            float damageTaken = updateCheck(ship, shieldState);
            float damageTakenShield = updateShieldLife(ship, shieldState, damageTaken);
            updateShieldRegen(amount, shieldState, ship, damageTakenShield);
            updateRender(amount, shieldState, ship);
            updateToolTipAndBoost(ship, engine, shieldState);
        }
    }

    private float updateCheck(ShipAPI ship, ShieldState shieldState) {
        float damageTaken = 0;
        damageTaken = shieldState.shipOldHitpoints - ship.getHitpoints();
        shieldState.shipOldHitpoints = ship.getHitpoints();

        if (damageTaken < 2) {
            damageTaken = 0;
        }

        ArmorGridAPI armorGrid = ship.getArmorGrid();
        float armorCell = 0;
        for (int x = 0; x < armorGrid.getGrid().length; x++) {
            for (int y = 0; y < armorGrid.getGrid()[x].length; y++) {
                armorCell = armorGrid.getArmorValue(x, y);
                damageTaken += shieldState.armor[x][y] - armorCell;
                shieldState.armor[x][y] = armorCell;
            }
        }
        return damageTaken;
    }

    private float updateShieldLife(ShipAPI ship, ShieldState shieldState, float damageTaken) {

        float percentLifeShield = shieldState.hitpoint / shieldState.maxhitpoint;
        shieldState.damageReduction = ((float) Math.sqrt(percentLifeShield) * REDUC_DAMAGE_BASE);

        // to 1, reduc to 0.8
        // to 0.2, reduc to 0.4
        float multiDamageTaken = shieldState.damageReduction / (1 - shieldState.damageReduction);
        float damageTakenShield = multiDamageTaken * damageTaken;
        // if(frame20==0)Global.getCombatEngine().getCombatUI().addMessage(0,"b:"+(damageTakenShield));

        if (shieldState.hitpoint > 0) {
            shieldState.hitpoint -= damageTakenShield;
            /* if (Global.getCombatEngine().getPlayerShip() != ship) {
                Global.getCombatEngine().addFloatingDamageText(ship.getLocation(), shieldState.hitpoint,
                        CRITICAL_SHIELD_COLOR, ship, ship);
            }*/
        }
        if (shieldState.hitpoint <= 0) {
            shieldState.hitpoint = 0;
        }

        return damageTakenShield;
    }

    private void updateShieldRegen(float amount, ShieldState shieldState, ShipAPI ship, float damageTakenShield) {
        // No regen if CR to zero.
        if ((ship.getCurrentCR() <= 0f && !ship.isFighter())) {
            return;
        }

        if (damageTakenShield <= shieldState.autoRegenLimitSelf) {
            shieldState.compteur += amount;
            if (shieldState.shieldRegenTimerSelf <= shieldState.compteur) {
                if (damageTakenShield != 0) {
                    shieldState.compteur = shieldState.shieldRegenTimerSelf;
                }

                shieldState.hitpoint += (shieldState.compteur - shieldState.shieldRegenTimerSelf + 1) * shieldState.lifeRegen;
                //if(frame20==0)Global.getCombatEngine().getCombatUI().addMessage(0,"c:"+((shieldState.compteur - shieldState.shieldRegenTimerSelf + 1) * shieldState.lifeRegen));

                if (shieldState.hitpoint > shieldState.maxhitpoint) {
                    shieldState.hitpoint = shieldState.maxhitpoint;
                    shieldState.compteur = 0;
                }
            }
        } else {
            shieldState.compteur = 0;
        }
    }

    private void updateRender(float amount, ShieldState shieldState, ShipAPI ship) {

        /* final Vector2f loc = ship.getLocation();
            glPushAttrib(GL_ENABLE_BIT);
            glMatrixMode(GL_PROJECTION);
            glPushMatrix();
            glViewport(0, 0, Display.getWidth(), Display.getHeight());
            glOrtho(0.0, Display.getWidth(), 0.0, Display.getHeight(), -1.0, 1.0);
            glEnable(GL_TEXTURE_2D);
            glEnable(GL_BLEND);
            sprite.setSize(radius, radius);
            sprite.setAlphaMult(effectLevel);
            sprite.renderAtCenter(view.convertWorldXtoScreenX(loc.x), view.convertWorldYtoScreenY(loc.y));
            sprite.setAngle(rotation);
            glPopMatrix();
            glPopAttrib();*/
        WeaponAPI shield = shieldState.shieldInter;
        float percentLife = shieldState.hitpoint / shieldState.maxhitpoint;
        float minimum = 0.3f + (float) FastTrig.sin(Global.getCombatEngine().getTotalElapsedTime(false) * 0.5f
                * Math.PI) * 0.05f;
        if (shieldState.damageReduction > 0f) {
            if (shieldState.glow < minimum) {
                shieldState.glow = Math.min(shieldState.glow + amount * 2f, minimum);
            } else {
                shieldState.glow = Math.max(shieldState.glow - amount * Math.max(shieldState.glow, 1f) * 0.75f,
                        minimum);
            }
        } else {
            shieldState.glow = Math.max(shieldState.glow - amount * 100f, 0f);
        }

        if (shield != null && shield.getAnimation() != null) {
            if (shieldState.glow > 0f) {
                shield.getAnimation().setFrame(1);
            } else {
                shield.getAnimation().setFrame(0);
            }
            shield.getAnimation().setAlphaMult(Math.min(shieldState.glow, 1f));
            shield.getSprite().setAdditiveBlend();

            if (percentLife < CRITICAL_THRESHOLD) {
                shield.getSprite().setColor(CRITICAL_SHIELD_COLOR);
            } else if (percentLife < WARNING_THRESHOLD) {
                shield.getSprite().setColor(FastTrig.sin(Global.getCombatEngine().getTotalElapsedTime(false)
                        * 4f * Math.PI) > 0f
                                ? CRITICAL_SHIELD_COLOR : VISUAL_SHIELD_COLOR);
            } else {
                shield.getSprite().setColor(VISUAL_SHIELD_COLOR);
            }
        }

    }

    private void updateToolTipAndBoost(ShipAPI ship, CombatEngineAPI engine, ShieldState shieldState) {
        final LocalData localData = (LocalData) engine.getCustomData().get(DATA_KEY);
        final Map<ShipAPI, Object[]> uiKey = localData.uiKey;

        if (ship == Global.getCombatEngine().getPlayerShip()) {
            if (!uiKey.containsKey(ship)) {
                Object[] array = new Object[2];
                for (int i = 0; i < array.length; i++) {
                    array[i] = new Object();
                }
                uiKey.put(ship, array);
            }
            /*
            if (ship.getPhaseCloak() != null) {
                if (ship.getPhaseCloak().getCooldownRemaining() > 0f) {
                    int rounded = Math.round(100 - 100 * (ship.getPhaseCloak().getCooldownRemaining() / ship.getPhaseCloak().getCooldown()));
                    
                    Global.getCombatEngine().maintainStatusForPlayerShip(uiKey.get(ship)[1],
                            "graphics/icons/hullsys/active_flare_launcher.png",
                            "Inner system", "" + rounded
                            + "% cooldown", true);
   
                } else {
                      Global.getCombatEngine().maintainStatusForPlayerShip(uiKey.get(ship)[1],
                            "graphics/icons/hullsys/active_flare_launcher.png",
                            "Inner system", "available", false);
           
                }
            }*/
        }
        if (shieldState.damageReduction > 0f) {
            if (ship == Global.getCombatEngine().getPlayerShip()) {
                Global.getCombatEngine().maintainStatusForPlayerShip(uiKey.get(ship)[0],
                        "graphics/icons/hullsys/fortress_shield.png",
                        "Lotus Shield", "" + Math.round(
                                shieldState.damageReduction * 100f)
                        + "% damage absorption", false);

            }

            ship.getMutableStats().getHighExplosiveDamageTakenMult().modifyMult(ID, 1 - shieldState.damageReduction);
            ship.getMutableStats().getKineticDamageTakenMult().modifyMult(ID, 1 + shieldState.damageReduction);
            ship.getMutableStats().getEnergyDamageTakenMult().modifyMult(ID, 1 - shieldState.damageReduction);
            ship.getMutableStats().getFragmentationDamageTakenMult().modifyMult(ID, 1 - shieldState.damageReduction);
            ship.getMutableStats().getEmpDamageTakenMult().modifyMult(ID, 1 - shieldState.damageReduction);
            //ship.getMutableStats().getBeamDamageTakenMult().modifyMult(ID, 1 - shieldState.damageReduction);
            ship.getMutableStats().getWeaponDamageTakenMult().modifyMult(ID, 1f - shieldState.damageReduction);
            ship.getMutableStats().getEngineDamageTakenMult().modifyMult(ID, 1f - shieldState.damageReduction);
        } else {
            if (ship == Global.getCombatEngine().getPlayerShip()) {
                Global.getCombatEngine().maintainStatusForPlayerShip(uiKey.get(ship)[0],
                        "graphics/icons/hullsys/fortress_shield.png",
                        "Lotus Shield Matrix", "Shields offline", false);
            }

            ship.getMutableStats().getHighExplosiveDamageTakenMult().unmodify(ID);
            ship.getMutableStats().getKineticDamageTakenMult().unmodify(ID);
            ship.getMutableStats().getEnergyDamageTakenMult().unmodify(ID);
            ship.getMutableStats().getFragmentationDamageTakenMult().unmodify(ID);
            ship.getMutableStats().getEmpDamageTakenMult().unmodify(ID);
            //ship.getMutableStats().getBeamDamageTakenMult().unmodify(ID);
            ship.getMutableStats().getWeaponDamageTakenMult().unmodify(ID);
            ship.getMutableStats().getEngineDamageTakenMult().unmodify(ID);
        }
    }

    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id) {
        for (String tmp : BLOCKED_HULLMODS) {
            if (ship.getVariant().getHullMods().contains(tmp)) {
                ship.getVariant().removeMod(tmp);
            }
        }
    }

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        float damageReduction = REDUC_DAMAGE_BASE;
        /* float shieldDamageHE = stats.getHighExplosiveShieldDamageTakenMult().base;
        float shieldDamageKin = stats.getKineticShieldDamageTakenMult().base;
        float shieldDamageFrag = stats.getFragmentationShieldDamageTakenMult().base;
         */

        stats.getHighExplosiveDamageTakenMult().modifyMult(ID, 1f - damageReduction);
        stats.getKineticDamageTakenMult().modifyMult(ID, 1f + damageReduction);
        stats.getEnergyDamageTakenMult().modifyMult(ID, 1f - damageReduction);
        stats.getFragmentationDamageTakenMult().modifyMult(ID, 1f - damageReduction);
        stats.getEmpDamageTakenMult().modifyMult(ID, 1f - damageReduction);
        //stats.getBeamDamageTakenMult().modifyMult(ID, 1f - damageReduction);
        stats.getWeaponDamageTakenMult().modifyMult(ID, 1f - damageReduction);
        stats.getEngineDamageTakenMult().modifyMult(ID, 1f - damageReduction);
    }

    @Override
    public String getDescriptionParam(int index, HullSize hullSize, ShipAPI ship) {
        if (index == 0) {
            return REDUC_DAMAGE_BASE * 100 + "%";
        }
        if (index == 1) {
            return REDUC_DAMAGE_BASE * 100 + "%";
        }
        if (index == 2) {
            return "" + ship.getMaxHitpoints() * PERCENT_LIFEMAX;
        }
        if (index == 3) {
            return "" + SHIELDREGENTIMER.get(ship.getHullSize());
        }
        return null;
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return ship != null && ship.getHullSpec().getHullId().startsWith("Art_");
    }

    public static final class ShieldState {

        float armor[][];
        public float hitpoint;
        public float maxhitpoint;
        float shipOldHitpoints;
        float glow = 0f;
        float compteur = 0f;
        float lifeRegen;
        float autoRegenLimitSelf;
        float shieldRegenTimerSelf;
        float offlineTimer = -1f;
        float onlineTimer = -1f;
        float warningTimer = -1f;
        float shipRadius = 0;
        WeaponAPI shieldInter;
        float damageReduction = REDUC_DAMAGE_BASE;

        private ShieldState(ShipAPI ship, float armor[][], float radius) {
            hitpoint = ship.getMaxHitpoints() * PERCENT_LIFEMAX;
            maxhitpoint = ship.getMaxHitpoints() * PERCENT_LIFEMAX;
            lifeRegen = ship.getMaxHitpoints() * PERCENT_LIFEREGEN;
            shipOldHitpoints = ship.getHitpoints();

            autoRegenLimitSelf = maxhitpoint * AUTOREGENLIMIT;//get(ship.getHullSize());
            shieldRegenTimerSelf = SHIELDREGENTIMER.get(ship.getHullSize());

            List<WeaponAPI> weapons = ship.getAllWeapons();
            for (WeaponAPI weapon : weapons) {
                if (weapon.getSlot().getId().equals("INTER")) {
                    shieldInter = weapon;
                    break;
                }
            }
            shipRadius = radius;

            this.armor = armor;
        }
    }

    private static final class LocalData {

        final Map<ShipAPI, Object[]> uiKey = new HashMap<>(60);
    }
}
