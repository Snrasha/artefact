package src.data.hullmods;

import com.fs.starfarer.api.Global;
import java.util.Map;
import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShieldAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.ids.Stats;
import com.fs.starfarer.api.util.IntervalUtil;
import java.util.HashMap;
import java.util.WeakHashMap;
import src.data.scripts.utils.Art_ShipArmorData;
import src.data.scripts.utils.Art_ShipKeyUtils;

public class Art_Artefact extends BaseHullMod {

    private static final Map<String, Art_ShipArmorData> ships = new WeakHashMap<>(100);
    private static final Map<String, IntervalUtil> timers = new WeakHashMap<>(100);
    private static final float BASICREGEN = 1.005f;
    private static final float HIGHTREGEN = 1.05f;
    public static final float PIERCE_MULT = 0.5f;
    public static final float SHIELD_BONUS = 25f;

    public static final String ID = "Art_Artefactshield";
    private static final String DATA_KEY = "Art_Artefactshield";

    private static final Map<ShipAPI, Art_Artefact.ShieldState> shipShield = new WeakHashMap<>(100);

    @Override
    public void advanceInCampaign(FleetMemberAPI member, float amount) {
        Art_ShipArmorData armorData = ships.get(member.getId());
        if (armorData != null && !armorData.CRSynched && armorData.totalCRLost > 0.0f) {
            armorData.RepairTracker = member.getRepairTracker();
            armorData.CRSynched = true;
        }
    }

    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id) {
        String shipKey = Art_ShipKeyUtils.getShipKey(ship);
        ships.put(shipKey, new Art_ShipArmorData(ship));
        timers.put(shipKey, new IntervalUtil(0.05f, 0.1f));
    }

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        stats.getCombatEngineRepairTimeMult().modifyMult(id, 0.3f);
        stats.getCombatWeaponRepairTimeMult().modifyMult(id, 0.3f);
    }

    @Override
    public void advanceInCombat(ShipAPI ship, float amount) {
        CombatEngineAPI engine = Global.getCombatEngine();

        if (engine == null || engine.isPaused()) {
            return;
        }

        ShieldAPI shield = ship.getShield();
        if (shield != null) {
            ShieldState shieldState = shipShield.get(ship);
            if (shield.isOn()) {
                float fluxLevel = ship.getFluxTracker().getFluxLevel();

                ship.getMutableStats().getShieldDamageTakenMult().modifyMult(ID, 1f - SHIELD_BONUS * 0.02f * (0.5f - fluxLevel));
                ship.getMutableStats().getDynamic().getStat(Stats.SHIELD_PIERCED_MULT).modifyMult(ID, PIERCE_MULT * (1 - fluxLevel));

                if (!engine.getCustomData().containsKey(DATA_KEY)) {
                    engine.getCustomData().put(DATA_KEY, new LocalData());
                }

                final LocalData localData = (LocalData) engine.getCustomData().get(DATA_KEY);
                final Map<ShipAPI, Object[]> uiKey = localData.uiKey;
                if (ship == Global.getCombatEngine().getPlayerShip()) {
                    if (!uiKey.containsKey(ship)) {
                        Object[] array = new Object[2];
                        for (int i = 0; i < array.length; i++) {
                            array[i] = new Object();
                        }
                        uiKey.put(ship, array);
                    }
                }

                if (ship == Global.getCombatEngine().getPlayerShip()) {
                    Global.getCombatEngine().maintainStatusForPlayerShip(uiKey.get(ship)[0],
                            "graphics/icons/hullsys/fortress_shield.png",
                            "Artefact Shield", "" + Math.round(
                                    (SHIELD_BONUS * 2f * (0.5f - fluxLevel)))
                            + "% shield absorption bonus", false);
                }

                if (shieldState != null) {

                } else {
                    shieldState = new ShieldState(1f, ship.getHullSpec().getShieldSpec().getRadius());
                    shipShield.put(ship, shieldState);
                    shield.setActiveArc(360);
                  /*  String inner;
                    String outer;

                    if (shieldState.shieldRadius >= 256.0f) {
                        inner = "graphics/fx/Art_shields256.png";
                        outer = "graphics/fx/Art_shields256ring.png";
                    } else if (shieldState.shieldRadius >= 128.0f) {
                        inner = "graphics/fx/Art_shields128.png";
                        outer = "graphics/fx/Art_shields128ring.png";
                    } else {
                        inner = "graphics/fx/Art_shields64.png";
                        outer = "graphics/fx/Art_shields64ring.png";
                    }
                    shield.setRadius(0, inner, outer);*/

                }
                shieldState.advance(amount);

                float radius = shieldState.onlineTimer * (shieldState.shieldRadius);

                String inner;
                String outer;

                if (radius >= 256.0f) {
                    inner = "graphics/fx/Art_shields256.png";
                    outer = "graphics/fx/Art_shields256ring.png";
                } else if (radius >= 128.0f) {
                    inner = "graphics/fx/Art_shields128.png";
                    outer = "graphics/fx/Art_shields128ring.png";
                } else {
                    inner = "graphics/fx/Art_shields64.png";
                    outer = "graphics/fx/Art_shields64ring.png";
                }
                //shield.setRadius(radius);

                shield.setRadius(radius, inner, outer);
                shield.setRingRotationRate(shield.getInnerRotationRate());

            } else if (shield.isOff()) {
                shipShield.remove(ship);
            }
        }

        int ii;
        float armorValue;
        int i;
        String shipKey = Art_ShipKeyUtils.getShipKey(ship);
        Art_ShipArmorData armorData = ships.get(shipKey);

        armorData.CRSynched = false;
        if (!armorData.initialArmorLoaded) {
            for (i = 0; i < armorData.armorCellsX; ++i) {
                for (ii = 0; ii < armorData.armorCellsY; ++ii) {
                    armorData.armorCells[i][ii] = armorData.armorGrid.getArmorValue(i, ii);
                }
            }
            armorData.initialArmorLoaded = true;
        }
        for (i = 0; i < armorData.armorCellsX; ++i) {
            for (ii = 0; ii < armorData.armorCellsY; ++ii) {
                armorValue = armorData.armorGrid.getArmorValue(i, ii);
                if (armorValue < armorData.armorCells[i][ii]) {
                    armorData.sinceLastDamage[i][ii] = 0.0f;
                    float[] arrf = armorData.expectedRepair[i];
                    int n = ii;
                    arrf[n] = arrf[n] + (armorData.armorCells[i][ii] - armorValue);
                } else {
                    float[] arrf = armorData.sinceLastDamage[i];
                    int n = ii;
                    arrf[n] = arrf[n] + amount;
                }
                armorData.armorCells[i][ii] = armorValue;
            }
        }

        for (i = 0; i < armorData.armorCellsX; ++i) {
            for (ii = 0; ii < armorData.armorCellsY; ++ii) {
                armorValue = armorData.armorGrid.getArmorValue(i, ii);
                if (armorData.sinceLastDamage[i][ii] <= 10.0f || armorValue >= armorData.maxArmor || armorData.expectedRepair[i][ii] <= 0.0f) {
                    continue;
                }
                float maxRepairFromExpected = armorData.expectedRepair[i][ii] * armorData.repairEfficiency;
                float maxRepairFromRate = amount * (0.2f + armorData.maxArmor * 0.1f);
                float repairAmount = Math.min(maxRepairFromExpected, maxRepairFromRate);
                repairAmount = Math.min(armorData.maxArmor - armorValue, repairAmount);
                armorData.armorGrid.setArmorValue(i, ii, armorValue + repairAmount);
                if (maxRepairFromExpected < maxRepairFromRate) {
                    armorData.expectedRepair[i][ii] = 0.0f;
                } else {
                    float[] arrf = armorData.expectedRepair[i];
                    int n = ii;
                    arrf[n] = arrf[n] - maxRepairFromRate / armorData.repairEfficiency;
                }
                armorData.totalRepaired += repairAmount;
            }
        }

        armorData.totalRepairedLast = armorData.totalRepaired;
        armorData.repairEfficiency = Math.max(0.8f * (1.0f - (float) Math.pow(armorData.totalRepaired / armorData.totalRepairCapacity, 2.0)), 0.0f);

        float seconds = Global.getSector().getClock().convertToDays(amount);

        IntervalUtil interval = timers.get(shipKey);

        interval.advance(seconds);
        if (interval.intervalElapsed()) {

            float maxhp = ship.getMaxHitpoints();
            float hp = ship.getHitpoints();
            if (hp > (maxhp * 0.3f)) {
                if (hp < (maxhp * ship.getCurrentCR())) {
                    ship.setHitpoints(hp * BASICREGEN);
                }
            } else {
                ship.setHitpoints(hp * HIGHTREGEN);
            }
        }
    }

    @Override
    public boolean isApplicableToShip(ShipAPI ship) {
        return true;
    }

    private static final class ShieldState {

        float onlineTimer;
        float multTime;
        float shieldRadius;

        private ShieldState(float multTime, float shieldRadius) {
            this.onlineTimer = 0f;
            this.multTime = multTime;
            this.shieldRadius = shieldRadius;
        }

        public void advance(float amount) {
            onlineTimer += amount * multTime;
            if (onlineTimer > 1f) {
                onlineTimer = 1f;
            }
        }

    }

    private static final class LocalData {

        final Map<ShipAPI, Object[]> uiKey = new HashMap<>(50);
    }
}
